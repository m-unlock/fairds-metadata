# FAIRDS-Metadata

**Metadata template used for the FAIRDS tool**
 - [FAIRDS repository](https://gitlab.com/m-unlock/fairds)
 - [Online App](https://fairds.fairbydesign.nl)

Each sheet in the metadata.xlsx file is converted to a TSV file to keep trackchanges.<br />

metadata.xlsx sheet can be found here: <br />
[http://download.systemsbiology.nl/unlock/metadata.xlsx](http://download.systemsbiology.nl/unlock/metadata.xlsx)


**Usual workflow**:<br />

Make changes in the excel file --> convert to TSV with convert_to_tsv.py --> commit the TSV's<br />


For more information on the FAIRDS and the templates [https://docs.fairbydesign.nl](https://docs.fairbydesign.nl/docs/index.html)