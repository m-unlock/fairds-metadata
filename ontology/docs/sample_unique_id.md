
# Term: sample unique ID
    
Unique identifier of the PDX or tumor sample, e.g. CRC00003


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sample_unique_id](http://fairbydesign.nl/ontology/sample_unique_id) |

