
# Term: sample phenotype
    
phenotype of the plant from which the sample was obtained, such as colour of corolla, fruit diameter, circular leaf shape; Plant Trait Ontology (TO), Phenotypic Quality Ontology (PATO), or other ontology is recommended; e.g. stem epidermis colour (TO:1000018)


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sample_phenotype](http://fairbydesign.nl/ontology/sample_phenotype) |

