
# Term: same as
    
An assay identifier of which this assay is an extension of (e.g. when multiple sequence runs / lanes are used). The same assay identifier needs to be used for this extra dataset


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `P12353`                             |
| URL             | [fairds:same_as](http://fairbydesign.nl/ontology/same_as) |

