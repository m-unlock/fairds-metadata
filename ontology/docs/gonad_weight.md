
# Term: gonad weight
    
Total weight of entire gonad tissue


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 g`                             |
| Unit            | `g`                                |
| URL             | [fairds:gonad_weight](http://fairbydesign.nl/ontology/gonad_weight) |

