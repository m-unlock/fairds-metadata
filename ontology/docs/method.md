
# Term: Method
    
Name of the method of observation


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `Growing degree days to anthesis`                             |
| URL             | [fairds:method](http://fairbydesign.nl/ontology/method) |

