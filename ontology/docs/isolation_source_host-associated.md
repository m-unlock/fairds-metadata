
# Term: isolation source host-associated
    
Name of host tissue or organ sampled for analysis. Example: tracheal tissue


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `tracheal tissue`                             |
| URL             | [fairds:isolation_source_host-associated](http://fairbydesign.nl/ontology/isolation_source_host-associated) |

