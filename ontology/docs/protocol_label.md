
# Term: Protocol Label
    
Identifies the protocol used to produce the sample, e.g. filtration and preservation. Example: BACT_NUC_W0.22-1.6.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:protocol_label](http://fairbydesign.nl/ontology/protocol_label) |

