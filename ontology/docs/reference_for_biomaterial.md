
# Term: reference for biomaterial
    
Primary publication if isolated before genome publication; otherwise, primary genome report. Mandatory for MIGS of bacteria and archaea.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:reference_for_biomaterial](http://fairbydesign.nl/ontology/reference_for_biomaterial) |

