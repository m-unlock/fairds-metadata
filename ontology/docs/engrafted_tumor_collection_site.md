
# Term: engrafted tumor collection site
    
If the sample origin is ‚Äúengrafted tumor‚Äù, please indicate the collection site from which the engrafted tumor sample was extracted (e.g. Liver). Please use terminology from NCIT ontology: https://www.ebi.ac.uk/ols/ontologies/ncit. If unknown please select NCIT term: ‚ÄúNot Available‚Äù ( http://purl.obolibrary.org/obo/NCIT_C126101)‚Äù. If the sample origin is ‚Äúpatient tumor‚Äù please do not use this attribute.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:engrafted_tumor_collection_site](http://fairbydesign.nl/ontology/engrafted_tumor_collection_site) |

