
# Term: Sampling Campaign
    
Refers to a finite or indefinite activity aiming at collecting data/samples, e.g. a cruise, a time series, a mesocosm experiment. Example: TARA_20110401Z.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sampling_campaign](http://fairbydesign.nl/ontology/sampling_campaign) |

