
# Term: illness symptoms
    
The symptoms that have been reported in relation to the illness, such as cough, diarrhea, fever, headache, malaise, myalgia, nausea, runny_nose, shortness_of_breath, sore_throat. If multiple exposures are applicable, please state them separated by semicolon.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `cough`                             |
| Unit            | `(cough|diarrhea|fever|headache|malaise|myalgia|nausea|runny_nose|shortness_of_breath|sore_throat)`                                |
| URL             | [fairds:illness_symptoms](http://fairbydesign.nl/ontology/illness_symptoms) |

