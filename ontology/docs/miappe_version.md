
# Term: MIAPPE version
    
The version of MIAPPE used.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `1.1`                             |
| URL             | [fairds:miappe_version](http://fairbydesign.nl/ontology/miappe_version) |

