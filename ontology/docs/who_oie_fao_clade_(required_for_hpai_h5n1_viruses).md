
# Term: WHO/OIE/FAO clade (required for HPAI H5N1 viruses)
    
WHO/OIE/FAO clade should be included for highly pathogenic H5N1 viruses. Example: 2.2


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:who_oie_fao_clade_(required_for_hpai_h5n1_viruses)](http://fairbydesign.nl/ontology/who_oie_fao_clade_(required_for_hpai_h5n1_viruses)) |

