
# Term: Slaughter Day
    
Date of slaughter


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `2023-06-06 00:00:00`                             |
| URL             | [fairds:slaughter_day](http://fairbydesign.nl/ontology/slaughter_day) |

