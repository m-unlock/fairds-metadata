
# Term: habitat
    
description of the location of the sample material. please use EnvO terms where possible: https://www.ebi.ac.uk/ols/ontologies/envo


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:habitat](http://fairbydesign.nl/ontology/habitat) |

