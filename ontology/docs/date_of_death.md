
# Term: date of death
    
Date of death of subject the sample was derived from.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `41358`                             |
| URL             | [fairds:date_of_death](http://fairbydesign.nl/ontology/date_of_death) |

