
# Term: Geographic location (country)
    
The country where the experiment took place, either as a full name or preferably as a 2-letter code.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `FR`                             |
| URL             | [fairds:geographic_location_(country)](http://fairbydesign.nl/ontology/geographic_location_(country)) |

