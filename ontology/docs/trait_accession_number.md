
# Term: Trait accession number
    
Accession number of the trait in a suitable controlled vocabulary (Crop Ontology, Trait Ontology).


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `CO_322:0000030; TO:0000366`                             |
| URL             | [fairds:trait_accession_number](http://fairbydesign.nl/ontology/trait_accession_number) |

