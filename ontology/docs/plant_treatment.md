
# Term: plant treatment
    
ontology term(s) that describes the plant treatment or relevant environmental conditions, recommend use of Plant Environment Ontology (EO) or other ontology, such as XEML Environment Ontology (XEO) or Crop Ontology (CO), more specific fields in the treatment section can be used in addition to or in place of this field


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:plant_treatment](http://fairbydesign.nl/ontology/plant_treatment) |

