
# Term: sample weight for DNA extraction
    
weight (g) of soil processed


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 g`                             |
| Unit            | `g`                                |
| URL             | [fairds:sample_weight_for_dna_extraction](http://fairbydesign.nl/ontology/sample_weight_for_dna_extraction) |

