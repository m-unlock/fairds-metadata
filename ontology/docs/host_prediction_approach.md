
# Term: host prediction approach
    
Tool or approach used for host prediction


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(CRISPR spacer match|co-occurrence|combination|host sequence similarity|kmer similarity|other|provirus)`                              |
| Example         | `combination`                             |
| URL             | [fairds:host_prediction_approach](http://fairbydesign.nl/ontology/host_prediction_approach) |

