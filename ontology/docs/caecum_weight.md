
# Term: Caecum Weight
    
Weight of empty caecum


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `2500g`                             |
| Unit            | `g`                                |
| URL             | [fairds:caecum_weight](http://fairbydesign.nl/ontology/caecum_weight) |

