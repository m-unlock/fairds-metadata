
# Term: library selection
    
Source ENA


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(RANDOM|PCR|RANDOM_PCR|RT-PCR|HMPR|MF|repeat_fractionation|size_fractionation|MSLL|cDNA|cDNA_randomPriming|cDNA_oligo_dT|PolyA|Oligo-dT|Inverse_rRNA|Inverse_rRNA_selection|ChIP|ChIP-Seq|MNase|DNase|Hybrid_Selection|Reduced_Representation|Restriction_Digest|5-methylcytidine_antibody|MBD2_protein_methyl-CpG_binding_domain|CAGE|RACE|MDA|padlock_probes_capture_method|other|unspecified)`                              |
| Example         | `RANDOM`                             |
| URL             | [fairds:library_selection](http://fairbydesign.nl/ontology/library_selection) |

