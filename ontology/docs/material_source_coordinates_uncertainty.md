
# Term: Material source coordinates uncertainty
    
Circular uncertainty of the coordinates, provided in meters (m). [Alternative identifier for in situ material]


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `200 m`                             |
| Unit            | `m`                                |
| URL             | [fairds:material_source_coordinates_uncertainty](http://fairbydesign.nl/ontology/material_source_coordinates_uncertainty) |

