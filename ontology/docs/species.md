
# Term: Species
    
Species name (formally: specific epithet) for the organism under study, according to standard scientific nomenclature.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `mays ; lycosperium x pennellii`                             |
| URL             | [fairds:species](http://fairbydesign.nl/ontology/species) |

