
# Term: influenza vaccination type
    
Influenza vaccinations that have been administered to the subject over the last year. Example: 2009 H1N1 Flumist


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `2009 H1N1 Flumist`                             |
| URL             | [fairds:influenza_vaccination_type](http://fairbydesign.nl/ontology/influenza_vaccination_type) |

