
# Term: influenza vaccination date
    
Date that the influenza vaccination was administered to the subject over the past year. Format: YYYY-MM-DD. Example: 2007-05-12


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `41358`                             |
| URL             | [fairds:influenza_vaccination_date](http://fairbydesign.nl/ontology/influenza_vaccination_date) |

