
# Term: patient age at collection of tumor
    
Age in years of the patient when the tumor was extracted. Please note this must be a whole number, e.g. 45


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `^150|1[0-4][0-9]|[1-9][0-9]|[0-9]|not provided$`                              |
| Example         | `45`                             |
| URL             | [fairds:patient_age_at_collection_of_tumor](http://fairbydesign.nl/ontology/patient_age_at_collection_of_tumor) |

