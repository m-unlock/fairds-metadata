
# Term: Material source other identifiers
    
Any other identifiers known to exist in other collections for this material source. Use key:value pairs, separated by semicolons.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `AGENT_ID:AB_06024; Yet_Another_ID:YAID_123`                             |
| URL             | [fairds:material_source_other_identifiers](http://fairbydesign.nl/ontology/material_source_other_identifiers) |

