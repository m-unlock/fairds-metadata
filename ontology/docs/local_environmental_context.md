
# Term: local environmental context
    
In this field, report the entity or entities which are in your sample or specimen‚Äôs local vicinity and which you believe have significant causal influences on your sample or specimen. EnvO terms can be found via the link: https://www.ebi.ac.uk/ols4


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:local_environmental_context](http://fairbydesign.nl/ontology/local_environmental_context) |

