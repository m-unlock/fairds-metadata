
# Term: illness onset date
    
Date the subject showed an onset of symptoms. Format: YYYY-MM-DD. Example: 2011-10-20


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `41330`                             |
| URL             | [fairds:illness_onset_date](http://fairbydesign.nl/ontology/illness_onset_date) |

