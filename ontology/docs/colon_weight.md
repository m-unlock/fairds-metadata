
# Term: Colon Weight
    
Weight of empty colon


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `4000g`                             |
| Unit            | `g`                                |
| URL             | [fairds:colon_weight](http://fairbydesign.nl/ontology/colon_weight) |

