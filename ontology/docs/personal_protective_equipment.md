
# Term: personal protective equipment
    
Use of personal protective equipment, such as gloves, gowns, during any type of exposure. Example: mask


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `mask`                             |
| URL             | [fairds:personal_protective_equipment](http://fairbydesign.nl/ontology/personal_protective_equipment) |

