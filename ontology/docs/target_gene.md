
# Term: target gene
    
Targeted gene or locus name for marker gene studies


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:target_gene](http://fairbydesign.nl/ontology/target_gene) |

