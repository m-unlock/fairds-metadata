
# Term: antiviral treatment initiation
    
Initiation of antiviral treatment after onset of clinical symptoms in days. Example: 2.5


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `2.5 days`                             |
| Unit            | `days`                                |
| URL             | [fairds:antiviral_treatment_initiation](http://fairbydesign.nl/ontology/antiviral_treatment_initiation) |

