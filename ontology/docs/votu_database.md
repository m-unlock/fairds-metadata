
# Term: vOTU database
    
"Reference database (i.e. sequences not generated as part of the current study) used to cluster new genomes in ""species-level"" vOTUs, if any. This should be formatted: {database};{version} e.g. NCBI Viral RefSeq;83"


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:votu_database](http://fairbydesign.nl/ontology/votu_database) |

