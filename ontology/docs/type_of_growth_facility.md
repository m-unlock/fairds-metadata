
# Term: Type of growth facility
    
Type of growth facility in which the study was carried out, in the form of an accession number from the Crop Ontology.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `CO_715:0000162`                             |
| URL             | [fairds:type_of_growth_facility](http://fairbydesign.nl/ontology/type_of_growth_facility) |

