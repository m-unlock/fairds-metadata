
# Term: engraftment host strain name
    
"If the sample origin is ‚Äúengrafted tumor‚Äù, please indicate the host mouse strain name from which the engrafted tumor sample was extracted (e.g. NOD.Cg-Prkdcscid Il2rgtm1Wjl/SzJ). Please use the following guidelines for the correct nomenclature format: http://www.informatics.jax.org/mgihome/nomen/strains.shtml#mice. If the sample origin is ‚Äúpatient tumor‚Äù please add ""not applicable"" and if unknown please add ""not provided""."


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:engraftment_host_strain_name](http://fairbydesign.nl/ontology/engraftment_host_strain_name) |

