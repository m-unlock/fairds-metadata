
# Term: size fraction selected
    
Filtering pore size used in sample preparation e.g. 0-0.22 micrometer


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:size_fraction_selected](http://fairbydesign.nl/ontology/size_fraction_selected) |

