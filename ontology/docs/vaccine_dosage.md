
# Term: vaccine dosage
    
Dosage of the vaccine taken by the subject. Example: 0.05 mL


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `2.1 ml`                             |
| Unit            | `ml`                                |
| URL             | [fairds:vaccine_dosage](http://fairbydesign.nl/ontology/vaccine_dosage) |

