
# Term: treatment dose
    
The dose of the treatment agent used.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 femtomolar`                             |
| Unit            | `femtomolar|micromolar|millimolar|molar|nanomolar|picomolar`                                |
| URL             | [fairds:treatment_dose](http://fairbydesign.nl/ontology/treatment_dose) |

