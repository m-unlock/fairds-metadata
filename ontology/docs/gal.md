
# Term: GAL
    
the name (or acronym) of the genome acquisition lab responsible for the sample.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(Centro Nacional de An√°lisis Gen√≥mico|DNA Sequencing and Genomics Laboratory, Helsinki Genomics Core Facility|Dalhousie University|Dresden-concept|Earlham Institute|Functional Genomics Center Zurich|GIGA-Genomics Core Facility, University of Liege|Genoscope|Geomar Helmholtz Centre|Hansen Lab, Denmark|Lausanne Genomic Technologies Facility|Leibniz Institute for the Analysis of Biodiversity Change, Museum Koenig, Bonn|Marine Biological Association|NGS Bern|NGS Competence Center T√ºbingen|Natural History Museum|Neuromics Support Facility, UAntwerp, VIB|Norwegian Sequencing Centre|Nova Southeastern University|Portland State University|Queen Mary University of London|Royal Botanic Garden Edinburgh|Royal Botanic Gardens Kew|Sanger Institute|SciLifeLab|Senckenberg Research Institute|Svardal Lab, Antwerp|The Sainsbury Laboratory|University of Bari|University of British Columbia|University of California|University of Cambridge|University of Derby|University of Edinburgh|University of Florence|University of Oregon|University of Oxford|University of Rhode Island|University of Vienna (Cephalopod)|University of Vienna (Mollusc)|West German Genome Centre|industry partner|other ERGA associated GAL)`                              |
| URL             | [fairds:gal](http://fairbydesign.nl/ontology/gal) |

