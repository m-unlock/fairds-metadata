
# Term: viral identification software
    
Tool(s) used for the identification of UViG as a viral genome, software or protocol name including version number, parameters, and cutoffs used formatted {software};{version};{parameters} e.g. VirSorter; 1.0.4; Virome database, category 2


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `VirSorter`                             |
| URL             | [fairds:viral_identification_software](http://fairbydesign.nl/ontology/viral_identification_software) |

