
# Term: sub_species
    
name of sub-species of organism from which sample was obtained


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sub_species](http://fairbydesign.nl/ontology/sub_species) |

