
# Term: population size of the catchment area
    
Refers to the number of people living in the area covered by the sewage system.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:population_size_of_the_catchment_area](http://fairbydesign.nl/ontology/population_size_of_the_catchment_area) |

