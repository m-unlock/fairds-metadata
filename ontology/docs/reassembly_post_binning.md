
# Term: reassembly post binning
    
Has an assembly been performed on a genome bin extracted from a metagenomic assembly?


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{boolean}`                              |
| Example         | `no`                             |
| URL             | [fairds:reassembly_post_binning](http://fairbydesign.nl/ontology/reassembly_post_binning) |

