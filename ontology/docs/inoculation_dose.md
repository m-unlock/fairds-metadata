
# Term: inoculation dose
    
Dose used for the inoculoation experiment.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:inoculation_dose](http://fairbydesign.nl/ontology/inoculation_dose) |

