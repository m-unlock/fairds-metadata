
# Term: Trait Characteristic Accession number
    
Accession number of the trait characteristic in a suitable controlled vocabulary (PATO - the Phenotype And Trait Ontology).


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `http://purl.obolibrary.org/obo/PATO_0001323`                             |
| URL             | [fairds:trait_characteristic_accession_number](http://fairbydesign.nl/ontology/trait_characteristic_accession_number) |

