
# Term: sample wet mass
    
measurement of wet mass at the time of sample collection; e.g. 0.23 g


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sample_wet_mass](http://fairbydesign.nl/ontology/sample_wet_mass) |

