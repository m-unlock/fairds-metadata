
# Term: Scale accession number
    
Accession number of the scale in a suitable controlled vocabulary (Crop Ontology).


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `CO_322:0000510`                             |
| URL             | [fairds:scale_accession_number](http://fairbydesign.nl/ontology/scale_accession_number) |

