
# Term: investigation type
    
Nucleic Acid Sequence Report is the root element of all MIxS compliant reports as standardised by Genomic Standards Consortium


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(bacteria_archaea|eukaryote|metagenome|metagenome-assembled genome|metatranscriptome|mimarks-specimen|mimarks-survey|organelle|plasmid|single amplified genome|uncultivated viral genomes|virus)`                              |
| Example         | `metagenome`                             |
| URL             | [fairds:investigation_type](http://fairbydesign.nl/ontology/investigation_type) |

