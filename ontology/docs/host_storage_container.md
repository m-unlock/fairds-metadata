
# Term: host storage container
    
storage container that the host organism is kept in before sampling (e.g. cage, water tank, pen) including details of size and type


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:host_storage_container](http://fairbydesign.nl/ontology/host_storage_container) |

