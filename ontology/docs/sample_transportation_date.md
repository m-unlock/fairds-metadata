
# Term: sample transportation date
    
transportation/shipping date of the sample. Format:YYYY-MM-DD


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| URL             | [fairds:sample_transportation_date](http://fairbydesign.nl/ontology/sample_transportation_date) |

