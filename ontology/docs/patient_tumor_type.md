
# Term: patient tumor type
    
For a primary sample (a tumor at the original site of origin), please select ‚ÄúPrimary Neoplasm‚Äù . For a metastatic sample (a tumor that has spread from its original (primary) site of growth to another site, close to or distant from the primary site), please select ‚ÄúMetastatic Neoplasm‚Äù . For a recurring neoplasm sample (neoplasm returning after a period of remission at the same location), please select ‚ÄúRecurrent Neoplasm‚Äù . If unknown tumor type, please select ‚Äúnot provided‚Äù.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(Metastatic Neoplasm|Primary Neoplasm|Recurrent Neoplasm|not provided)`                              |
| Example         | `Primary Neoplasm`                             |
| URL             | [fairds:patient_tumor_type](http://fairbydesign.nl/ontology/patient_tumor_type) |

