
# Term: broad-scale environmental context
    
In this field, report which major environmental system your sample or specimen came from. The systems identified should have a coarse spatial grain, to provide the general environmental context of where the sampling was done (e.g. were you in the desert or a rainforest?). EnvO terms can be found via the link: https://www.ebi.ac.uk/ols4


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:broad-scale_environmental_context](http://fairbydesign.nl/ontology/broad-scale_environmental_context) |

