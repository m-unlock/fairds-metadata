
# Term: sample transportation temperature
    
transportation temperature from sample site to storage


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `25 ºC`                             |
| Unit            | `ºC`                                |
| URL             | [fairds:sample_transportation_temperature](http://fairbydesign.nl/ontology/sample_transportation_temperature) |

