
# Term: serotype
    
serological variety of a species characterized by its antigenic properties


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:serotype](http://fairbydesign.nl/ontology/serotype) |

