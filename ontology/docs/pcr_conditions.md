
# Term: pcr conditions
    
Description of reaction conditions and components for PCR in the form of 'initial denaturation:94degC_1.5min; annealing=...'


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:pcr_conditions](http://fairbydesign.nl/ontology/pcr_conditions) |

