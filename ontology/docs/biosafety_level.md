
# Term: biosafety level
    
Biosafety level of the corresponding sample


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(1|2|3|4|unknown)`                              |
| Example         | `2`                             |
| URL             | [fairds:biosafety_level](http://fairbydesign.nl/ontology/biosafety_level) |

