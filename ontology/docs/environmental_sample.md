
# Term: environmental_sample
    
identifies sequences derived by direct molecular isolation from a bulk environmental DNA sample (by PCR with or without subsequent cloning of the product, DGGE, or other anonymous methods) with no reliable identification of the source organism


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{boolean}`                              |
| Example         | `true`                             |
| URL             | [fairds:environmental_sample](http://fairbydesign.nl/ontology/environmental_sample) |

