
# Term: antiviral treatment duration
    
Duration of antiviral treatment after onset of clinical symptoms in days.Example: 5


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 days`                             |
| Unit            | `days`                                |
| URL             | [fairds:antiviral_treatment_duration](http://fairbydesign.nl/ontology/antiviral_treatment_duration) |

