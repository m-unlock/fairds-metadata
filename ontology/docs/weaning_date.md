
# Term: Weaning Date
    
Date at which weaning starts


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{datetime}`                              |
| Example         | `2023-05-15 00:00:00`                             |
| URL             | [fairds:weaning_date](http://fairbydesign.nl/ontology/weaning_date) |

