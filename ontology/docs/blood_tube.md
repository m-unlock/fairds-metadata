
# Term: Blood Tube
    
Type of anticoagulant: EDTA or Heparin


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(EDTA|Heparin)`                              |
| Example         | `EDTA`                             |
| URL             | [fairds:blood_tube](http://fairbydesign.nl/ontology/blood_tube) |

