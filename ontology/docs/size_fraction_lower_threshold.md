
# Term: Size Fraction Lower Threshold
    
SAMPLE_PROTOCOL_Size-Fraction_Lower-Threshold_(µm) indicates the lower size threshold. Materials smaller than the size threshold are excluded from the sample. Example: 0.22 µm


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:size_fraction_lower_threshold](http://fairbydesign.nl/ontology/size_fraction_lower_threshold) |

