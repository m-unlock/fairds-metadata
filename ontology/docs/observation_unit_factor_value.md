
# Term: Observation Unit factor value
    
List of values for each factor applied to the observation unit.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `Watered`                             |
| URL             | [fairds:observation_unit_factor_value](http://fairbydesign.nl/ontology/observation_unit_factor_value) |

