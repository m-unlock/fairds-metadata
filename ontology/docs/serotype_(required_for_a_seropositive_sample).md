
# Term: serotype (required for a seropositive sample)
    
Serological variety of a species characterised by its antigenic properties. For Influenza, HA subtype should be the letter H followed by a number between 1-16 unless novel subtype is identified and the NA subtype should be the letter N followed by a number between 1-9 unless novel subtype is identified. If only one of the subtypes have been tested then use the format H5Nx or HxN1. Example: H1N1


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:serotype_(required_for_a_seropositive_sample)](http://fairbydesign.nl/ontology/serotype_(required_for_a_seropositive_sample)) |

