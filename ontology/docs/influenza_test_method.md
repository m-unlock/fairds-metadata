
# Term: influenza test method
    
Method by which the current assessment of a sample as flu positive/negative is made. If multiple test were performed, please state them separated by semicolon. Example: RT-PCR; antigen ELISA


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `RT-PCR`                             |
| URL             | [fairds:influenza_test_method](http://fairbydesign.nl/ontology/influenza_test_method) |

