
# Term: patient sex
    
Sex of the patient from which the tumor was extracted. If sex is unknown please select ‚Äúnot available‚Äù


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(Female|Male|not available)`                              |
| Example         | `not available`                             |
| URL             | [fairds:patient_sex](http://fairbydesign.nl/ontology/patient_sex) |

