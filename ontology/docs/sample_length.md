
# Term: sample length
    
length of subject at the time of sampling, if different from the height; e.g. 2m


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sample_length](http://fairbydesign.nl/ontology/sample_length) |

