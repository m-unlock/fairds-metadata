
# Term: tissue_lib
    
tissue library from which sample was obtained


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:tissue_lib](http://fairbydesign.nl/ontology/tissue_lib) |

