
# Term: Chlorophyll Sensor
    
Fluorescence of the water measured in volts and converted to milligrammes of chlorophyll per cubic metre. Format: ##.####, SDN:P02:75:CPWC, SDN:P06:46:UMMC for mg Chl/m3. Example: 0.066.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 mg Chl/m3`                             |
| Unit            | `mg Chl/m3`                                |
| URL             | [fairds:chlorophyll_sensor](http://fairbydesign.nl/ontology/chlorophyll_sensor) |

