
# Term: sequence quality check
    
Indicate if the sequence has been called by automatic systems (none) or undergone a manual editing procedure (e.g. by inspecting the raw data or chromatograms). Applied only for sequences that are not submitted to SRA or DRA


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(manual|none|software)`                              |
| Example         | `software`                             |
| URL             | [fairds:sequence_quality_check](http://fairbydesign.nl/ontology/sequence_quality_check) |

