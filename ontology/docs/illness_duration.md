
# Term: illness duration
    
The number of days the illness lasted. Example: 4


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 days`                             |
| Unit            | `days|hours|months|weeks|years`                                |
| URL             | [fairds:illness_duration](http://fairbydesign.nl/ontology/illness_duration) |

