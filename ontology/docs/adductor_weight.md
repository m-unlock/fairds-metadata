
# Term: adductor weight
    
Total weight of striated muscle and smooth muscle


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 g`                             |
| Unit            | `g`                                |
| URL             | [fairds:adductor_weight](http://fairbydesign.nl/ontology/adductor_weight) |

