
# Term: Citation
    
Citation of the Sample Registry (HTML version) at the PANGAEA. Example: doi.pangaea.de/10.1594/PANGAEA.76752.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `^(doi\:)?\d{2}\.\d{4}.*$`                              |
| URL             | [fairds:citation](http://fairbydesign.nl/ontology/citation) |

