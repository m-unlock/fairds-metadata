
# Term: host gutted mass
    
total mass of the host after gutting, the unit depends on host


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 g`                             |
| Unit            | `g|kg`                                |
| URL             | [fairds:host_gutted_mass](http://fairbydesign.nl/ontology/host_gutted_mass) |

