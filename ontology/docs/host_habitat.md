
# Term: host habitat
    
Natural habitat of the avian or mammalian host.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(domestic:free-range farm|domestic:indoor farm|domestic:live market|domestic:semi-enclosed housing|other|wild:migratory|wild:resident)`                              |
| Example         | `domestic:indoor farm`                             |
| URL             | [fairds:host_habitat](http://fairbydesign.nl/ontology/host_habitat) |

