
# Term: Nitrate Sensor
    
Nitrate concentration parameters in the water column measured in volts and converted to micromoles per litres. Format: ##.####, SDN:P02:75:NTRA, SDN:P06:46:UPOX for µmol/L. Example: 0.2259.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 µmol/L`                             |
| Unit            | `µmol/L`                                |
| URL             | [fairds:nitrate_sensor](http://fairbydesign.nl/ontology/nitrate_sensor) |

