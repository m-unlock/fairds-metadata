
# Term: original geographic location
    
"For use if the specimen is from a zoo, botanic garden or culture collection etc. and has a known origin elsewhere. Please record the general description of the original collection location. This should be formatted as a country and optionally include more specific locations ranging from least to most specific separated by a | character, e.g., United Kingdom | East Anglia | Norfolk | Norwich | University of East Anglia | UEA Broad""."


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `United Kingdom `                             |
| URL             | [fairds:original_geographic_location](http://fairbydesign.nl/ontology/original_geographic_location) |

