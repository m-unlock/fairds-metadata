
# Term: patient tumor diagnosis at time of collection
    
"Patient tumor diagnosis at time of collection for engraftment in PDX model or organoid/cell derivation. Please use terminology from NCIT ontology: https://www.ebi.ac.uk/ols/ontologies/ncit - Please note in NCIT ontology, usually the ‚Äúcancer‚Äù concept is represented with ‚Äúmalignant neoplasm‚Äùexample: ‚Äúlung cancer‚Äù is ‚ÄúMalignant Lung Neoplasm‚Äù (http://purl.obolibrary.org/obo/NCIT_C7377). If precise diagnosis is unknown, please use ""Neoplasm"" (http://purl.obolibrary.org/obo/NCIT_C3262)"


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:patient_tumor_diagnosis_at_time_of_collection](http://fairbydesign.nl/ontology/patient_tumor_diagnosis_at_time_of_collection) |

