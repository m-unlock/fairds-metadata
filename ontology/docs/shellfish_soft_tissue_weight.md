
# Term: shellfish soft tissue weight
    
Total weight of all soft tissue, i.e. weight of entire organism without shell, at the time of sampling


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 g`                             |
| Unit            | `g`                                |
| URL             | [fairds:shellfish_soft_tissue_weight](http://fairbydesign.nl/ontology/shellfish_soft_tissue_weight) |

