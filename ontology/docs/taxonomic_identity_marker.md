
# Term: taxonomic identity marker
    
The phylogenetic marker(s) used to assign an organism name to the bin, SAG or MAG. Examples are 16S gene, multi-marker approach or other e.g. rpoB gene


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `rpoB gene`                             |
| URL             | [fairds:taxonomic_identity_marker](http://fairbydesign.nl/ontology/taxonomic_identity_marker) |

