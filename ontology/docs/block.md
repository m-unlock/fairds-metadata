
# Term: block
    
A block or batch is an experimental unit arrangement into a group which is similar to one another. Typically, a blocking factor is a source of variability that is not of primary interest to the experimenter. An example of a blocking factor might be the sex of a patient; by blocking on sex, this source of variability is controlled for, thus leading to greater accuracy (EFO_0005067).


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| URL             | [fairds:block](http://fairbydesign.nl/ontology/block) |

