
# Term: sample material processing
    
Any processing applied to the sample during or after retrieving the sample from environment. This field accepts OBI, for a browser of OBI (v 2013-10-25) terms please see http://purl.bioontology.org/ontology/OBI


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sample_material_processing](http://fairbydesign.nl/ontology/sample_material_processing) |

