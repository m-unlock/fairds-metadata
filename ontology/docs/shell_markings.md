
# Term: shell markings
    
Visible markings on outer shell


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `5 g`                             |
| Unit            | `g`                                |
| URL             | [fairds:shell_markings](http://fairbydesign.nl/ontology/shell_markings) |

