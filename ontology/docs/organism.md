
# Term: Organism
    
An identifier for the organism at the species level. Use of the NCBI taxon ID is recommended.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `NCBITAXON:4577`                             |
| URL             | [fairds:organism](http://fairbydesign.nl/ontology/organism) |

