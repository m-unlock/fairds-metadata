
# Term: antiviral treatment
    
Antiviral treatment used for this subject, such as Zanamavir Oseltamivir, Amantadine. Example: Rimantadine


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(Zanamavir|Oseltamivir|Amantadine|Rimantadine)`                              |
| Example         | `Rimantadine`                             |
| URL             | [fairds:antiviral_treatment](http://fairbydesign.nl/ontology/antiviral_treatment) |

