
# Term: plant body site
    
name of body site that the sample was obtained from. For Plant Ontology (PO) (v 20) terms, see http://purl.bioontology.org/ontology/PO


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:plant_body_site](http://fairbydesign.nl/ontology/plant_body_site) |

