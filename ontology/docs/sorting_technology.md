
# Term: sorting technology
    
Method used to sort/isolate cells or particles of interest


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(flow cytometric cell sorting|lazer-tweezing|microfluidics|micromanipulation|optical manipulation|other)`                              |
| Example         | `lazer-tweezing`                             |
| URL             | [fairds:sorting_technology](http://fairbydesign.nl/ontology/sorting_technology) |

