
# Term: sampling strategy
    
Strategy used to perform the sampling procedure


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `strategy X`                             |
| URL             | [fairds:sampling_strategy](http://fairbydesign.nl/ontology/sampling_strategy) |

