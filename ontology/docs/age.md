
# Term: age
    
Age of the organism the sample was derived from.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 days`                             |
| Unit            | `centuries|days|decades|hours|minutes|months|seconds|weeks|years`                                |
| URL             | [fairds:age](http://fairbydesign.nl/ontology/age) |

