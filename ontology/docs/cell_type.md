
# Term: cell_type
    
cell type from which the sample was obtained


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:cell_type](http://fairbydesign.nl/ontology/cell_type) |

