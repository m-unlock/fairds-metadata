
# Term: sample storage conditions
    
Conditions at which sample was stored, usually storage temperature, duration and location


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sample_storage_conditions](http://fairbydesign.nl/ontology/sample_storage_conditions) |

