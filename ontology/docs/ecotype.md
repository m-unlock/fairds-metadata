
# Term: ecotype
    
a population within a given species displaying genetically based, phenotypic traits that reflect adaptation to a local habitat.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:ecotype](http://fairbydesign.nl/ontology/ecotype) |

