
# Term: lab_host
    
scientific name of the laboratory host used to propagate the source organism from which the sample was obtained


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:lab_host](http://fairbydesign.nl/ontology/lab_host) |

