
# Term: sub_strain
    
name or identifier of a genetically or otherwise modified strain from which sample was obtained, derived from a parental strain (which should be annotated in the strain field; sub_strain from which sample was obtained


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sub_strain](http://fairbydesign.nl/ontology/sub_strain) |

