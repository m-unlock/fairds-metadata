
# Term: sample dry mass
    
measurement of dry mass at the time of sample collection; e.g. 0.05g


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sample_dry_mass](http://fairbydesign.nl/ontology/sample_dry_mass) |

