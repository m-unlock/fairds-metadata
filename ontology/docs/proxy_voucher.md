
# Term: proxy voucher
    
For use if voucher material needs to be made from a specimen that is different than the one submitted for sequencing. Please record the unique identifier that references the physical specimen and that ideally exists in a curated collection. The ID should have the following structure: name of the institution (institution code) followed by the collection code (if available) and the voucher id (institution_code:collection_code:voucher_id)


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:proxy_voucher](http://fairbydesign.nl/ontology/proxy_voucher) |

