
# Term: PenID
    
Pen identification number


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `\b\d{4}\b`                              |
| Example         | `1124`                             |
| URL             | [fairds:penid](http://fairbydesign.nl/ontology/penid) |

