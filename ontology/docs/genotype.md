
# Term: genotype
    
name or code for genotype of organism


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:genotype](http://fairbydesign.nl/ontology/genotype) |

