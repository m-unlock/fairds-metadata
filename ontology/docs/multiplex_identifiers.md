
# Term: multiplex identifiers
    
Molecular barcodes, called Multiplex Identifiers (MIDs), that are used to specifically tag unique samples in a sequencing run. Sequence should be reported in uppercase letters


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:multiplex_identifiers](http://fairbydesign.nl/ontology/multiplex_identifiers) |

