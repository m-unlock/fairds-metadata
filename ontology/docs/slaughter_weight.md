
# Term: Slaughter Weight
    
Weight at slaughter


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `100kg`                             |
| Unit            | `kg`                                |
| URL             | [fairds:slaughter_weight](http://fairbydesign.nl/ontology/slaughter_weight) |

