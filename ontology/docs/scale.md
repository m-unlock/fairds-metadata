
# Term: Scale
    
Name of the scale associated with the variable


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `°C day`                             |
| URL             | [fairds:scale](http://fairbydesign.nl/ontology/scale) |

