
# Term: Days of Treatment
    
The number of days treatment is administered


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `4`                             |
| URL             | [fairds:days_of_treatment](http://fairbydesign.nl/ontology/days_of_treatment) |

