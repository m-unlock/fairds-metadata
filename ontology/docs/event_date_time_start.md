
# Term: Event Date/Time Start
    
Date and time in UTC when the sampling event started, e.g. each CTD cast, net tow, or bucket collection is a distinct event. Format: yyyy-mm-ddThh:mm:ssZ. Example: 2011-04-16T15:05:30Z.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{datetime}`                              |
| URL             | [fairds:event_date_time_start](http://fairbydesign.nl/ontology/event_date_time_start) |

