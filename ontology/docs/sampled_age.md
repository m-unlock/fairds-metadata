
# Term: sampled age
    
age of subject at the time of sample collection; relevant scale depends on species and study; e.g. 2 weeks old


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sampled_age](http://fairbydesign.nl/ontology/sampled_age) |

