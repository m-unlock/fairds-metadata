
# Term: Flowcell
    
Flowcell type used for sequencing


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `FLO-MIN106, FLO-PRO111`                             |
| URL             | [fairds:flowcell](http://fairbydesign.nl/ontology/flowcell) |

