
# Term: extrachromosomal elements
    
Do plasmids exist of significant phenotypic consequence (e.g. ones that determine virulence or antibiotic resistance). Megaplasmids? Other plasmids (borrelia has 15+ plasmids).


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| URL             | [fairds:extrachromosomal_elements](http://fairbydesign.nl/ontology/extrachromosomal_elements) |

