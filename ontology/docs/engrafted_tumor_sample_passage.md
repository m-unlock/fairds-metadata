
# Term: engrafted tumor sample passage
    
"If engrafted tumor sample, please indicate the passage from which the engrafted tumor sample was harvested (passage 0 must be the first engraftment in the mouse). Please ensure you add a non-negative number greater than 0. If the sample origin is ‚Äúpatient tumor‚Äù please enter ""not applicable"". If passage number is unknown please enter ""not provided‚Äù."


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:engrafted_tumor_sample_passage](http://fairbydesign.nl/ontology/engrafted_tumor_sample_passage) |

