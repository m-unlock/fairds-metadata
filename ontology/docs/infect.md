
# Term: infect
    
The name of the disease causing/contaminating organism; the value can also be control


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:infect](http://fairbydesign.nl/ontology/infect) |

