
# Term: Observation unit type
    
Type of observation unit in textual form, usually one of the following: study, block, sub-block, plot, sub-plot, pot, plant. Use of other observation unit types is possible but not recommended. The observation unit type cannot be used to indicate sub-plant levels. However, observations can still be made on the sub-plant level, as long as the details are indicated in the associated observed variable (see observed variables). Alternatively, it is possible to use samples for more detailed tracing of sub-plant units, attaching the observations to them instead.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `plot`                             |
| URL             | [fairds:observation_unit_type](http://fairbydesign.nl/ontology/observation_unit_type) |

