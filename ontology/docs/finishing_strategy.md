
# Term: finishing strategy
    
Was the genome project intended to produce a complete or draft genome, Coverage, the fold coverage of the sequencing expressed as 2x, 3x, 18x etc, and how many contigs were produced for the genome. Mandatory for MIGS of eukaryote, bacteria and archaea.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:finishing_strategy](http://fairbydesign.nl/ontology/finishing_strategy) |

