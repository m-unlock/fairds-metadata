
# Term: WGA amplification kit
    
Kit used to amplify genomic DNA in preparation for sequencing


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:wga_amplification_kit](http://fairbydesign.nl/ontology/wga_amplification_kit) |

