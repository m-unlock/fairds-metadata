
# Term: Material source accession name
    
"Can be: (i)genebank accession registered name or other designation given to the material, other than the donor""s accession number or collecting number. (ii) Variety name."


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `Rheinische Vorgebirgstrauben`                             |
| URL             | [fairds:material_source_accession_name](http://fairbydesign.nl/ontology/material_source_accession_name) |

