
# Term: growth media
    
information about growth media for growing the plants or tissue cultured samples


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:growth_media](http://fairbydesign.nl/ontology/growth_media) |

