
# Term: End date of study
    
Date and, if relevant, time when the experiment ended


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `2002-11-27 00:00:00`                             |
| URL             | [fairds:end_date_of_study](http://fairbydesign.nl/ontology/end_date_of_study) |

