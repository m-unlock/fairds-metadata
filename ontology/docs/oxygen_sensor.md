
# Term: Oxygen Sensor
    
Oxygen concentration parameters in the water column measured in volts and converted to micromoles per kilogrammes. Format: ###.####, SDN:P02:75:DOXY, SDN:P06:46:KGUM for µmol/kg. Example: 217.7895.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 µmol/kg`                             |
| Unit            | `µmol/kg`                                |
| URL             | [fairds:oxygen_sensor](http://fairbydesign.nl/ontology/oxygen_sensor) |

