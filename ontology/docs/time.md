
# Term: time
    
The duration in which the treatment has occurred.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `3 days`                             |
| Unit            | `days|hours|minutes|weeks|years`                                |
| URL             | [fairds:time](http://fairbydesign.nl/ontology/time) |

