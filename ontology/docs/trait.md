
# Term: Trait
    
Name of the (plant or environmental) trait under observation


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `Anthesis time;  Reproductive growth time`                             |
| URL             | [fairds:trait](http://fairbydesign.nl/ontology/trait) |

