
# Term: Observation unit level hierarchy
    
Hierarchy of the different levels of repetitions between each others


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `block>rep>plot`                             |
| URL             | [fairds:observation_unit_level_hierarchy](http://fairbydesign.nl/ontology/observation_unit_level_hierarchy) |

