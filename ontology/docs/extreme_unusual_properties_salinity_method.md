
# Term: extreme_unusual_properties/salinity method
    
reference or method used in determining salinity


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:extreme_unusual_properties_salinity_method](http://fairbydesign.nl/ontology/extreme_unusual_properties_salinity_method) |

