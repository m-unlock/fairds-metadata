
# Term: environmental stress
    
Environmental stress is a treatment where some aspect of the environment is perturbed in order to stress the organism or culture, e.g. change in temperature, change in watering regime.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:environmental_stress](http://fairbydesign.nl/ontology/environmental_stress) |

