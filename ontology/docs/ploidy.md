
# Term: ploidy
    
The ploidy level of the genome (e.g. allopolyploid, haploid, diploid, triploid, tetraploid). It has implications for the downstream study of duplicated gene and regions of the genomes (and perhaps for difficulties in assembly). For terms, please select terms listed under class ploidy (PATO:001374) of Phenotypic Quality Ontology (PATO), and for a browser of PATO (v 2013-10-28) please refer to http://purl.bioontology.org/ontology/PATO. Mandatory for MIGS of eukaryotes.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `allopolyploid`                             |
| URL             | [fairds:ploidy](http://fairbydesign.nl/ontology/ploidy) |

