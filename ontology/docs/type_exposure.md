
# Term: type exposure
    
Setting within which the subject is exposed to animals, such as farm, slaughterhouse, food preparation. If multiple exposures are applicable, please state their type in the same order in which you reported the exposure in the field 'subject exposure'. Example: backyard flock; confined animal feeding operation


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:type_exposure](http://fairbydesign.nl/ontology/type_exposure) |

