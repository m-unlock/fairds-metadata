
# Term: culture_collection
    
institution code and identifier for the culture from which the sample was obtained, with optional collection code.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:culture_collection](http://fairbydesign.nl/ontology/culture_collection) |

