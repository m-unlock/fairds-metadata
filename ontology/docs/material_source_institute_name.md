
# Term: Material source institute name
    
Name of the material source institute.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `INRA`                             |
| URL             | [fairds:material_source_institute_name](http://fairbydesign.nl/ontology/material_source_institute_name) |

