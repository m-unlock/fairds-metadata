
# Term: respirable particulate matter
    
concentration of substances that remain suspended in the air, and comprise mixtures of organic and inorganic substances (PM10 and PM2.5); can report multiple PM's by entering numeric values preceded by name of PM


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5µg/m3`                             |
| Unit            | `µg/m3`                                |
| URL             | [fairds:respirable_particulate_matter](http://fairbydesign.nl/ontology/respirable_particulate_matter) |

