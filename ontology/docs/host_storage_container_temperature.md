
# Term: host storage container temperature
    
temperature at which the host is kept before sampling


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `25ºC`                             |
| Unit            | `ºC`                                |
| URL             | [fairds:host_storage_container_temperature](http://fairbydesign.nl/ontology/host_storage_container_temperature) |

