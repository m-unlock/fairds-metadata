
# Term: aquaculture origin
    
Origin of stock and raised conditions


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(AOAR - aquacultue origin aquaculture raised|WOAR - wild origin aquaculture raised|WOWR - wild origin wild raised)`                              |
| Example         | `AOAR - aquacultue origin aquaculture raised`                             |
| URL             | [fairds:aquaculture_origin](http://fairbydesign.nl/ontology/aquaculture_origin) |

