
# Term: tRNA extraction software
    
Tools used for tRNA identification. Add names and versions of software(s), parameters used


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:trna_extraction_software](http://fairbydesign.nl/ontology/trna_extraction_software) |

