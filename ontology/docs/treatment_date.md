
# Term: treatment date
    
The date of the treatment applied.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `41417`                             |
| URL             | [fairds:treatment_date](http://fairbydesign.nl/ontology/treatment_date) |

