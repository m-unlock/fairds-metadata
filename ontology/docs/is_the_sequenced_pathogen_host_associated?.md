
# Term: Is the sequenced pathogen host associated?
    
Is the sequenced pathogen host associated? ('Yes' or 'No')


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{boolean}`                              |
| Example         | `false`                             |
| URL             | [fairds:is_the_sequenced_pathogen_host_associated?](http://fairbydesign.nl/ontology/is_the_sequenced_pathogen_host_associated?) |

