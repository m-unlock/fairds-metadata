
# Term: LitterID
    
Litter identification number (in case of cross-fostering)


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `25`                             |
| URL             | [fairds:litterid](http://fairbydesign.nl/ontology/litterid) |

