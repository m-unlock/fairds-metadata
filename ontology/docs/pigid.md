
# Term: PigID
    
Pig identification number


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `51651`                             |
| URL             | [fairds:pigid](http://fairbydesign.nl/ontology/pigid) |

