
# Term: isolate
    
individual isolate from which the sample was obtained


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:isolate](http://fairbydesign.nl/ontology/isolate) |

