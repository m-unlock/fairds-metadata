
# Term: Time scale
    
Name of the scale or unit of time with which observations of this type were recorded in the data file (for time series studies).


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `Growing degree day (GDD); Date/Time`                             |
| URL             | [fairds:time_scale](http://fairbydesign.nl/ontology/time_scale) |

