
# Term: seabed habitat
    
Classification of the seabed where the organism has been found; for European seabed habitats please use terms from http://eunis.eea.europa.eu/habitats-code-browser.jsp; example: B3.4 : Soft sea-cliffs, often vegetated


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:seabed_habitat](http://fairbydesign.nl/ontology/seabed_habitat) |

