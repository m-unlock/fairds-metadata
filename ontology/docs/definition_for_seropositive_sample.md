
# Term: definition for seropositive sample
    
The cut off value used by an investigatior in determining that a sample was seropositive.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:definition_for_seropositive_sample](http://fairbydesign.nl/ontology/definition_for_seropositive_sample) |

