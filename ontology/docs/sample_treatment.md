
# Term: sample treatment
    
Treatment applied to the sample


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `treatment X`                             |
| URL             | [fairds:sample_treatment](http://fairbydesign.nl/ontology/sample_treatment) |

