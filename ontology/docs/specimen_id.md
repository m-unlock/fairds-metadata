
# Term: specimen_id
    
Unique identifier used to link all data for the recorded specimen.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:specimen_id](http://fairbydesign.nl/ontology/specimen_id) |

