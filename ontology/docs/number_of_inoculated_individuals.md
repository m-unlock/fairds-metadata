
# Term: number of inoculated individuals
    
Number of host individuals inoculated for the experiment.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `5`                             |
| URL             | [fairds:number_of_inoculated_individuals](http://fairbydesign.nl/ontology/number_of_inoculated_individuals) |

