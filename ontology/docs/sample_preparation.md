
# Term: sample preparation
    
A description of the sample preparation step performed


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `filtering at 2um`                             |
| URL             | [fairds:sample_preparation](http://fairbydesign.nl/ontology/sample_preparation) |

