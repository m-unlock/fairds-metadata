
# Term: propagation
    
This field is specific to different taxa. For plants: sexual/asexual, for phages: lytic/lysogenic, for plasmids: incompatibility group (Note: there is the strong opinion to name phage propagation obligately lytic or temperate, therefore we also give this choice. Mandatory for MIGS of eukaryotes, plasmids and viruses.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:propagation](http://fairbydesign.nl/ontology/propagation) |

