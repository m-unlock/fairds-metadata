
# Term: Material source latitude
    
Latitude of the material source. [Alternative identifier for in situ material]


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(^{number}.?[0-9]{0,8}$)|(^not collected$)|(^not provided$)|(^restricted access$)`                              |
| Example         | `39.067`                             |
| URL             | [fairds:material_source_latitude](http://fairbydesign.nl/ontology/material_source_latitude) |

