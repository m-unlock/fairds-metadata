
# Term: patient tumor primary site
    
Site of the primary tumor where cancer originates (may not correspond to the site of the collected tissue sample). Please use NCIT ontology, e.g. Colon (http://purl.obolibrary.org/obo/NCIT_C12382). If unknown please select NCIT term: ‚ÄúNot Available‚Äù ( http://purl.obolibrary.org/obo/NCIT_C126101)


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:patient_tumor_primary_site](http://fairbydesign.nl/ontology/patient_tumor_primary_site) |

