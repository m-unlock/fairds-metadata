
# Term: virus enrichment approach
    
List of approaches used to enrich the sample for viruses, if any


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(CsCl density gradient|DNAse|FeCl Precipitation|PEG Precipitation|RNAse|centrifugation|filtration|none|other|targeted sequence capture|ultracentrifugation|ultrafiltration)`                              |
| Example         | `DNAse`                             |
| URL             | [fairds:virus_enrichment_approach](http://fairbydesign.nl/ontology/virus_enrichment_approach) |

