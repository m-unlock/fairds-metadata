
# Term: sample height
    
height of subject at the time of sampling, if different from the length; e.g. 0.75m


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sample_height](http://fairbydesign.nl/ontology/sample_height) |

