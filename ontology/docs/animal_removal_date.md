
# Term: Animal Removal Date
    
Date of pig removal from the study due to unplanned reasons


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `2023-12-07 00:00:00`                             |
| URL             | [fairds:animal_removal_date](http://fairbydesign.nl/ontology/animal_removal_date) |

