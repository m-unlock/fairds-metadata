
# Term: Sample Type
    
The type of sample collected


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(blood|saliva|feces|tissue|hair|digesta|mucosa|histology)`                              |
| Example         | `blood`                             |
| URL             | [fairds:sample_type](http://fairbydesign.nl/ontology/sample_type) |

