
# Term: sample disease status
    
list of diseases with which the subject has been diagnosed at the time of sample collection; can include multiple diagnoses; the value of the field depends on subject; e.g. Charcoal rot (Macrophomina phaseolina), Late wilt (Cephalosporium maydis)


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sample_disease_status](http://fairbydesign.nl/ontology/sample_disease_status) |

