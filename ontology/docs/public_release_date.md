
# Term: Public release date
    
Date of first public release of the dataset presently being described.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `2013-02-25 00:00:00`                             |
| URL             | [fairds:public_release_date](http://fairbydesign.nl/ontology/public_release_date) |

