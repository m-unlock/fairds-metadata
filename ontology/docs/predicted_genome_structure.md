
# Term: predicted genome structure
    
Expected structure of the viral genome


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(non-segmented|segmented|undetermined)`                              |
| Example         | `segmented`                             |
| URL             | [fairds:predicted_genome_structure](http://fairbydesign.nl/ontology/predicted_genome_structure) |

