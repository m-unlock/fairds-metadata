
# Term: collector name
    
Name of the person who collected the specimen. Example: John Smith


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:collector_name](http://fairbydesign.nl/ontology/collector_name) |

