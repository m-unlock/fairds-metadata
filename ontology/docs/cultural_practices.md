
# Term: Cultural practices
    
General description of the cultural practices of the study.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `Irrigation was applied according needs during summer to prevent water stress.`                             |
| URL             | [fairds:cultural_practices](http://fairbydesign.nl/ontology/cultural_practices) |

