
# Term: library source
    
Source ENA


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(GENOMIC|GENOMIC_SINGLE_CELL|TRANSCRIPTOMIC|TRANSCRIPTOMIC_SINGLE_CELL|METAGENOMIC|METATRANSCRIPTOMIC|SYNTHETIC|VIRAL_RNA|OTHER)`                              |
| Example         | `GENOMIC`                             |
| URL             | [fairds:library_source](http://fairbydesign.nl/ontology/library_source) |

