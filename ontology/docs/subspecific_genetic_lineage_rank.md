
# Term: subspecific genetic lineage rank
    
further information about the genetic distinctness of this lineage by recording additional information i.e. variety, cultivar, ecotype, inbred line,; it can also contain alternative taxonomic information


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:subspecific_genetic_lineage_rank](http://fairbydesign.nl/ontology/subspecific_genetic_lineage_rank) |

