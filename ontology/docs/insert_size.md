
# Term: Insert size
    
Insert size for paired reads


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{integer}`                              |
| Example         | `500`                             |
| URL             | [fairds:insert_size](http://fairbydesign.nl/ontology/insert_size) |

