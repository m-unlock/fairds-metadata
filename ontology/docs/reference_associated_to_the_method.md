
# Term: Reference associated to the method
    
URI/DOI of reference describing the method.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{url}`                              |
| Example         | `https://doi.org/10.2134/agronmonogr31.c2`                             |
| URL             | [fairds:reference_associated_to_the_method](http://fairbydesign.nl/ontology/reference_associated_to_the_method) |

