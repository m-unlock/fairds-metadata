
# Term: Sampling Platform
    
Refers to the unique stage from which the sampling device has been deployed. Includes Platform category from SDN:L06, http://seadatanet.maris2.nl/v_bodc_vocab_v2/search.asp?lib=L06, and Platform name. Example: Research Vessel Tara.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sampling_platform](http://fairbydesign.nl/ontology/sampling_platform) |

