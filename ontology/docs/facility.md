
# Term: Facility
    
The facility that was used for generating the data


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `facility name`                             |
| URL             | [fairds:facility](http://fairbydesign.nl/ontology/facility) |

