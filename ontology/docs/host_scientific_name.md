
# Term: host scientific name
    
Scientific name of the natural (as opposed to laboratory) host to the organism from which sample was obtained.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `homo sapiens`                             |
| URL             | [fairds:host_scientific_name](http://fairbydesign.nl/ontology/host_scientific_name) |

