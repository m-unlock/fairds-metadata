
# Term: growth condition
    
A role that a material entity can play which enables particular conditions used to grow organisms or parts of the organism. This includes isolated environments such as cultures and open environments such as field studies.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:growth_condition](http://fairbydesign.nl/ontology/growth_condition) |

