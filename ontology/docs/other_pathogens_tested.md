
# Term: other pathogens tested
    
Classification of pathogenic organisms other than influenza virus tested in the current assessment of a sample. If multiple tests were performed, please state them separated by semicolon. If no other pathogens test was performed, please state 'none'. Example: Newcastle


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:other_pathogens_tested](http://fairbydesign.nl/ontology/other_pathogens_tested) |

