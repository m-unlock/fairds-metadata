
# Term: License
    
License for the reuse of the data associated with this investigation. The Creative Commons licenses cover most use cases and are recommended.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `CC BY-SA 4.0 ; Unreported`                             |
| URL             | [fairds:license](http://fairbydesign.nl/ontology/license) |

