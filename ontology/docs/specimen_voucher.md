
# Term: specimen_voucher
    
Unique identifier that references the physical specimen that remains after the sequence has been obtained and that ideally exists in a curated collection.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:specimen_voucher](http://fairbydesign.nl/ontology/specimen_voucher) |

