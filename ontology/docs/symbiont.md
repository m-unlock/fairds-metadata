
# Term: symbiont
    
Used to separate host and symbiont metadata within a symbiont system where the host species are indicated as 'N' and symbionts are indicated as 'Y'


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{boolean}`                              |
| URL             | [fairds:symbiont](http://fairbydesign.nl/ontology/symbiont) |

