
# Term: ORCID
    
The orcid of a given person


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `^\d{4}-\d{4}-\d{4}-\d{3}(\d|X)$`                              |
| Example         | `0000-0003-2125-060X`                             |
| URL             | [fairds:orcid](http://fairbydesign.nl/ontology/orcid) |

