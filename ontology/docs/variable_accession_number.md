
# Term: Variable accession number
    
Accession number of the variable in the Crop Ontology


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `CO_322:0000794`                             |
| URL             | [fairds:variable_accession_number](http://fairbydesign.nl/ontology/variable_accession_number) |

