
# Term: Birth Weight
    
Individual birth weight of piglets


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `1460g`                             |
| Unit            | `g`                                |
| URL             | [fairds:birth_weight](http://fairbydesign.nl/ontology/birth_weight) |

