
# Term: Event Date/Time End
    
Date and time in UTC when the sampling event ended, e.g. each CTD cast, net tow, or bucket collection is a distinct event. Format: yyyy-mm-ddThh:mm:ssZ. Example: 2011-04-16T15:38:00Z.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{datetime}`                              |
| URL             | [fairds:event_date_time_end](http://fairbydesign.nl/ontology/event_date_time_end) |

