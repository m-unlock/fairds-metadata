
# Term: individual
    
An individual used a specimen in an experiment, from which a material sample was derived.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:individual](http://fairbydesign.nl/ontology/individual) |

