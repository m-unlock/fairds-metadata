
# Term: Type of Enrichment
    
Specific type of enrichment used


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `^.{1,50}$`                              |
| Example         | `hay`                             |
| URL             | [fairds:type_of_enrichment](http://fairbydesign.nl/ontology/type_of_enrichment) |

