
# Term: pathotype
    
name or code for pathotype of organism


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:pathotype](http://fairbydesign.nl/ontology/pathotype) |

