
# Term: vOTU sequence comparison approach
    
"Tool and thresholds used to compare sequences when computing ""species-level"" vOTUs formatted: {software};{version};{parameters} e.g. gblastn;2.6.0+;e-value cutoff: 0.001"


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:votu_sequence_comparison_approach](http://fairbydesign.nl/ontology/votu_sequence_comparison_approach) |

