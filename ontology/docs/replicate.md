
# Term: replicate
    
A role played by a a biological sample in the context of an experiment where the intent is that biological or technical variation is measured.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:replicate](http://fairbydesign.nl/ontology/replicate) |

