
# Term: chemical compound
    
A drug, solvent, chemical, etc., with a property that can be measured such as concentration (http://purl.obolibrary.org/obo/CHEBI_37577).


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:chemical_compound](http://fairbydesign.nl/ontology/chemical_compound) |

