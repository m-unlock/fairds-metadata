
# Term: protocol
    
The protocol used for preparation or isolation (name or url) used to create the assay


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `http://example.com`                             |
| URL             | [fairds:protocol](http://fairbydesign.nl/ontology/protocol) |

