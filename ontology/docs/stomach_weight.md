
# Term: Stomach Weight
    
Weight of empty stomach


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5000g`                             |
| Unit            | `g`                                |
| URL             | [fairds:stomach_weight](http://fairbydesign.nl/ontology/stomach_weight) |

