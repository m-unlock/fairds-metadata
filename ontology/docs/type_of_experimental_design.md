
# Term: Type of experimental design
    
Type of experimental  design of the study, in the form of an accession number from the Crop Ontology.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `CO_715:0000145`                             |
| URL             | [fairds:type_of_experimental_design](http://fairbydesign.nl/ontology/type_of_experimental_design) |

