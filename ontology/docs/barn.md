
# Term: Barn
    
Barn unit number


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `\b\d{4}\b`                              |
| Example         | `3608`                             |
| URL             | [fairds:barn](http://fairbydesign.nl/ontology/barn) |

