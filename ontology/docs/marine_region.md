
# Term: Marine Region
    
The geographical origin of the sample as defined by the marine region name chosen from the Marine Regions vocabulary at http://www.marineregions.org/. Example: Aegean Sea.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:marine_region](http://fairbydesign.nl/ontology/marine_region) |

