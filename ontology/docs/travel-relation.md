
# Term: travel-relation
    
Designates the relation of the main diagnosis to the patient's travel.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(not ascertainable|not travel-related|travel-related)`                              |
| Example         | `not travel-related`                             |
| URL             | [fairds:travel-relation](http://fairbydesign.nl/ontology/travel-relation) |

