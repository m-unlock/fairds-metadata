
# Term: cellular component
    
The part of a cell or its extracellular environment in which a gene product is located.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:cellular_component](http://fairbydesign.nl/ontology/cellular_component) |

