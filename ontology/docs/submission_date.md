
# Term: Submission Date
    
Date of submission of the dataset presently being described to a host repository.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `2012-12-17 00:00:00`                             |
| URL             | [fairds:submission_date](http://fairbydesign.nl/ontology/submission_date) |

