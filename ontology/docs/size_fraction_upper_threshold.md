
# Term: Size Fraction Upper Threshold
    
SAMPLE_PROTOCOL_Size-Fraction_Upper-Threshold_(µm) indicates the upper size threshold. Materials larger than the size threshold are excluded from the sample.Example: 1.6 µm


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:size_fraction_upper_threshold](http://fairbydesign.nl/ontology/size_fraction_upper_threshold) |

