
# Term: relationship
    
indicates if the specimen has a known relationship to another specimen (e.g. parental, child, sibling or other kind of relationship)


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:relationship](http://fairbydesign.nl/ontology/relationship) |

