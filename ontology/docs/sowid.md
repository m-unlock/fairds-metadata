
# Term: SowID
    
Sow identification number (born from)


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `51651`                             |
| URL             | [fairds:sowid](http://fairbydesign.nl/ontology/sowid) |

