
# Term: Material source institute code
    
FAO WIEWS code of the institute where the accession is maintained. The current set of institute codes is available from https://www.fao.org/wiews. If no institute code is available, create your own (Laboratory acronym or research institute acronym ...).


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `FRA09`                             |
| URL             | [fairds:material_source_institute_code](http://fairbydesign.nl/ontology/material_source_institute_code) |

