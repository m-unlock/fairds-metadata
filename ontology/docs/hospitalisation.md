
# Term: hospitalisation
    
Was the subject confined to a hospital as a result of virus infection or problems occurring secondary to virus infection?


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{boolean}`                              |
| URL             | [fairds:hospitalisation](http://fairbydesign.nl/ontology/hospitalisation) |

