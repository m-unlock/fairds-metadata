
# Term: Fecal Score (Barn)
    
Fecal consistency scored by barn personnel: 1=Firm and shaped, 2=Soft and shaped, 3=Loose, 4=Watery


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(1|2|3|4)`                              |
| Example         | `1`                             |
| URL             | [fairds:fecal_score_(barn)](http://fairbydesign.nl/ontology/fecal_score_(barn)) |

