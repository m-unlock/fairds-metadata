
# Term: Biological material latitude
    
Latitude of the studied biological material. [Alternative identifier for in situ material]


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(^{number}.?[0-9]{0,8}$)|(^not collected$)|(^not provided$)|(^restricted access$)`                              |
| Example         | `39.067`                             |
| URL             | [fairds:biological_material_latitude](http://fairbydesign.nl/ontology/biological_material_latitude) |

