
# Term: lifestage
    
the age class or life stage of the organism at the time of collection.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(adult|egg|embryo|gametophyte|juvenile|larva|not applicable|not collected|not provided|pupa|spore-bearing structure|sporophyte|vegetative cell|vegetative structure|zygote)`                              |
| Example         | `larva`                             |
| URL             | [fairds:lifestage](http://fairbydesign.nl/ontology/lifestage) |

