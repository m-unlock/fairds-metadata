
# Term: Weaning Weight
    
Individual body weight at weaning


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `6000g`                             |
| Unit            | `g`                                |
| URL             | [fairds:weaning_weight](http://fairbydesign.nl/ontology/weaning_weight) |

