
# Term: feature prediction
    
Method used to predict UViGs features such as ORFs, integration site, etc. Add names and versions of software(s), parameters used


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:feature_prediction](http://fairbydesign.nl/ontology/feature_prediction) |

