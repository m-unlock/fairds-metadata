
# Term: sub_type
    
name of sub-type of organism from which sample was obtained


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sub_type](http://fairbydesign.nl/ontology/sub_type) |

