
# Term: host diet treatment concentration
    
Concentration of experimental diet treatment/additive to normal host feed as a percentage of the mass of the feed (e.g. 20%)


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `20 % mass`                             |
| Unit            | `% mass`                                |
| URL             | [fairds:host_diet_treatment_concentration](http://fairbydesign.nl/ontology/host_diet_treatment_concentration) |

