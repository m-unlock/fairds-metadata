
# Term: antiviral treatment dosage
    
Dosage of the treatment taken by the subject. Example: 0.05 mg


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `0.05 mg`                             |
| Unit            | `mg|g`                                |
| URL             | [fairds:antiviral_treatment_dosage](http://fairbydesign.nl/ontology/antiviral_treatment_dosage) |

