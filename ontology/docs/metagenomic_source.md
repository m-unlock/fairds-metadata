
# Term: metagenomic source
    
The metagenomic source of the sample. This value should contain metagenome and be in the taxonomy database e.g. wastewater metagenome or human gut metagenome. Please note, metagenome, alone will not be accepted. Check here for more details on metagenome taxonomy: https://ena-docs.readthedocs.io/en/latest/faq_taxonomy.html#environmental-taxonomic-classifications


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:metagenomic_source](http://fairbydesign.nl/ontology/metagenomic_source) |

