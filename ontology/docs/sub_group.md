
# Term: sub_group
    
name of sub-group of organism from which sample was obtained


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sub_group](http://fairbydesign.nl/ontology/sub_group) |

