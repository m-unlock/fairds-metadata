
# Term: shellfish total weight
    
Total weight of shellfish including shell at the time of sampling. Epifauna and epiphytes to be removed.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 g`                             |
| Unit            | `g`                                |
| URL             | [fairds:shellfish_total_weight](http://fairbydesign.nl/ontology/shellfish_total_weight) |

