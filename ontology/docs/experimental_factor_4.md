
# Term: experimental factor 4
    
A variable that the experiment is based on.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(RNA|age|block|cell line|cell type|chemical compound|cultivar|dev_stage|disease staging|dose|ecotype|environmental history|environmental stress|genotype|growth condition|immunoprecipitate|individual|infect|initial time point|organism|organism part|phenotype|ploidy|protein-bound|protocol|replicate|rnase|sample type|sampling site|sampling time point|sex|strain|temperature|time|transgene)`                              |
| URL             | [fairds:experimental_factor_4](http://fairbydesign.nl/ontology/experimental_factor_4) |

