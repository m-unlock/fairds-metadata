
# Term: predicted genome type
    
Type of genome predicted for the UViG


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(DNA|RNA|dsDNA|dsRNA|mixed|ssDNA|ssRNA|ssRNA (\+)|ssRNA (-)|uncharacterized)`                              |
| Example         | `DNA`                             |
| URL             | [fairds:predicted_genome_type](http://fairbydesign.nl/ontology/predicted_genome_type) |

