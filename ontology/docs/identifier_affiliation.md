
# Term: identifier_affiliation
    
the university, institution, or society responsible for identifying the specimen.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `Wageningen University`                             |
| URL             | [fairds:identifier_affiliation](http://fairbydesign.nl/ontology/identifier_affiliation) |

