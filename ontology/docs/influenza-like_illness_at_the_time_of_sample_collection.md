
# Term: influenza-like illness at the time of sample collection
    
Is the subject at the time of sample collection considered to have influenza like illness?


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{boolean}`                              |
| Example         | `true`                             |
| URL             | [fairds:influenza-like_illness_at_the_time_of_sample_collection](http://fairbydesign.nl/ontology/influenza-like_illness_at_the_time_of_sample_collection) |

