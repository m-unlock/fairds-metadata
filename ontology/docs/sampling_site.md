
# Term: Sampling Site
    
Refers to the site/station where data/sample collection is performed. Term list: OSD Site Registry or SDN:C17, http://tinyurl.com/oxux985 Example: Poseidon-E1-M3A Time Series Station.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sampling_site](http://fairbydesign.nl/ontology/sampling_site) |

