
# Term: influenza test result
    
Classification of a sample as flu positive or negative based on the test performed and reported. If multiple tests were performed, please state the results in the same order in which you reported the tests in the field 'influenza test method'. Format: P(ositive)/N(egative). Example: P; P


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{boolean}`                              |
| Example         | `true`                             |
| URL             | [fairds:influenza_test_result](http://fairbydesign.nl/ontology/influenza_test_result) |

