
# Term: virus identifier
    
Unique laboratory identifier assigned to the virus by the investigator. Strain name is not sufficient since it might not be unique due to various passsages of the same virus. Format: up to 50 alphanumeric characters


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:virus_identifier](http://fairbydesign.nl/ontology/virus_identifier) |

