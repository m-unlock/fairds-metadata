
# Term: shell length
    
length of shell (perpendicular to the hinge)


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 g`                             |
| Unit            | `g`                                |
| URL             | [fairds:shell_length](http://fairbydesign.nl/ontology/shell_length) |

