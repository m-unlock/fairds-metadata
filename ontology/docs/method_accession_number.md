
# Term: Method accession number
    
Accession number of the method in a suitable controlled vocabulary (Crop Ontology, Trait Ontology).


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `CO_322:0000189`                             |
| URL             | [fairds:method_accession_number](http://fairbydesign.nl/ontology/method_accession_number) |

