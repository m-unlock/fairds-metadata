
# Term: mating_type
    
mating type of the organism from which the sequence was obtained; mating type is used for prokaryotes, and for eukaryotes that undergo meiosis without sexually dimorphic gametes


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:mating_type](http://fairbydesign.nl/ontology/mating_type) |

