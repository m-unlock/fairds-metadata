
# Term: Cytokine
    
Type of cytokine measured


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(IFN-Y|IL-1|IL-6|IL-10|IL-12|TNF-a)`                              |
| Example         | `IL-6`                             |
| URL             | [fairds:cytokine](http://fairbydesign.nl/ontology/cytokine) |

