
# Term: Description of growth facility
    
Short description of the facility in which the study was carried out.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `field environment condition ; NA`                             |
| URL             | [fairds:description_of_growth_facility](http://fairbydesign.nl/ontology/description_of_growth_facility) |

