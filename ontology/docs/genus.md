
# Term: Genus
    
Genus name for the organism under study, according to standard scientific nomenclature.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `Zea ; Solanum`                             |
| URL             | [fairds:genus](http://fairbydesign.nl/ontology/genus) |

