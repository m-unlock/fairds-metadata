
# Term: culture_or_strain_id
    
living, culturable, named laboratory strain that sequenced material is derived from.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:culture_or_strain_id](http://fairbydesign.nl/ontology/culture_or_strain_id) |

