
# Term: host disease outcome
    
Disease outcome in the host.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(dead|recovered|recovered with sequelae)`                              |
| URL             | [fairds:host_disease_outcome](http://fairbydesign.nl/ontology/host_disease_outcome) |

