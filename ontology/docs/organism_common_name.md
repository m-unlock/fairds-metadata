
# Term: organism common name
    
common name of the subject organism, e.g. maize


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:organism_common_name](http://fairbydesign.nl/ontology/organism_common_name) |

