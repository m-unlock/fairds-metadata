
# Term: Biological material external ID
    
One to many identifiers for the biological material. Can include EBI Biosamples ID. URI are recommended when possible.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `plot:894`                             |
| URL             | [fairds:biological_material_external_id](http://fairbydesign.nl/ontology/biological_material_external_id) |

