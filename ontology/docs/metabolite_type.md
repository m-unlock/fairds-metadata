
# Term: Metabolite Type
    
Type of metabolite analyzed


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(Ammonia|Biogenic amines|Phenol|Indoles|Organic acids|VFA|Hydrogen sulphide)`                              |
| Example         | `Ammonia`                             |
| URL             | [fairds:metabolite_type](http://fairbydesign.nl/ontology/metabolite_type) |

