
# Term: Feed Intake
    
Daily feed ingested by the pig


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `206g`                             |
| Unit            | `g`                                |
| URL             | [fairds:feed_intake](http://fairbydesign.nl/ontology/feed_intake) |

