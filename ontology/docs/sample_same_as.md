
# Term: sample same as
    
Reference to sample(s) that are equivalent. The referenced sample(s) should already be registered in INSDC. This should be formatted as one of the following. A single sample e.g. ERSxxxxxx OR a comma separated list e.g. ERSxxxxxx,ERSxxxxxx


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(^[ESD]RS\d{6,}(,[ESD]RS\d{6,})*$)|(^SAM[END][AG]?\d+(,SAM[END][AG]?\d+)*$)|(^EGAN\d{11}(,EGAN\d{11})*$)`                              |
| URL             | [fairds:sample_same_as](http://fairbydesign.nl/ontology/sample_same_as) |

