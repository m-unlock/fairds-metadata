
# Term: clinical setting
    
The timing of the clinic visit in relation to travel.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(after travel|during travel)`                              |
| URL             | [fairds:clinical_setting](http://fairbydesign.nl/ontology/clinical_setting) |

