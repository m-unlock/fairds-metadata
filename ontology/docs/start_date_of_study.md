
# Term: Start date of study
    
Date and, if relevant, time when the experiment started


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `2002-04-04 ; 2006-09-27T10:23:21+00:00`                             |
| URL             | [fairds:start_date_of_study](http://fairbydesign.nl/ontology/start_date_of_study) |

