
# Term: creepfed
    
Creepfeed provided during the suckling period


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{boolean}`                              |
| Example         | `yes`                             |
| URL             | [fairds:creepfed](http://fairbydesign.nl/ontology/creepfed) |

