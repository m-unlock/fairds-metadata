
# Term: composite design/sieving (if any)
    
collection design of pooled samples and/or sieve size and amount of sample sieved


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:composite_design_sieving_(if_any)](http://fairbydesign.nl/ontology/composite_design_sieving_(if_any)) |

