
# Term: sample coordinator affiliation
    
The university, institution, or society affiliation of the sample coordinator.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sample_coordinator_affiliation](http://fairbydesign.nl/ontology/sample_coordinator_affiliation) |

