
# Term: Platform
    
The platform that was used for generating the data (Source: https://ena-docs.readthedocs.io/en/latest/submit/reads/webin-cli.html#permitted-values-for-platform)


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(LS454|ILLUMINA|PACBIO_SMRT|ION_TORRENT|CAPILLARY|OXFORD_NANOPORE|BGISEQ|DNBSEQ)`                              |
| Example         | `ILLUMINA`                             |
| URL             | [fairds:platform](http://fairbydesign.nl/ontology/platform) |

