
# Term: Hematology Parameter
    
Parameters investigated in hematology analysis: RBC (red blood cells), HGB (hemoglobin), HCT (hematocrit), MCV (mean cell volume), MCH (mean corpuscular hemoglobin), MCHC (mean corpuscular hemoglobin concentration), RET (reticulocytes), PLT (platelets), WBC (white blood cells), NEUT (neutrophils), LYMPH (lymphocytes), MONO (monocytes), EOS (eosinophils), BASO (basophils)


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(RBC|HGB|HCT|MCV|MCH|MCHC|RET|PLT|WBC|NEUT|LYMPH|MONO|EO|BASO)`                              |
| Example         | `HCT`                             |
| URL             | [fairds:hematology_parameter](http://fairbydesign.nl/ontology/hematology_parameter) |

