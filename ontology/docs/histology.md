
# Term: Histology
    
Type of histological measurement


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(Villi height|Crypt depth|VH:CD|Muscle thickness|Neutral mucins|Acidic mucins)`                              |
| Example         | `Crypt depth`                             |
| URL             | [fairds:histology](http://fairbydesign.nl/ontology/histology) |

