
# Term: isolation source non-host-associated
    
Describes the physical, environmental and/or local geographical source of the biological sample from which the sample was derived. Example: soil


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `soil`                             |
| URL             | [fairds:isolation_source_non-host-associated](http://fairbydesign.nl/ontology/isolation_source_non-host-associated) |

