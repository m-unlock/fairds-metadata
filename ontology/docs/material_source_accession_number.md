
# Term: Material source accession number
    
Unique identifier for accessions within a genebank. If material source is not from a genebank, use a laboratory ID. In the case of a commercial variety, use the variety code, or name if no code available.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `W95115_inra; SYNTHETICS R93; APACHE`                             |
| URL             | [fairds:material_source_accession_number](http://fairbydesign.nl/ontology/material_source_accession_number) |

