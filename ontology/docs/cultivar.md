
# Term: cultivar
    
cultivar (cultivated variety) of plant from which sample was obtained


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:cultivar](http://fairbydesign.nl/ontology/cultivar) |

