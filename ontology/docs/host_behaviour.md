
# Term: host behaviour
    
Natural behaviour of the host.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(captive-wild (e.g. at zoo)|domestic|other|wild)`                              |
| URL             | [fairds:host_behaviour](http://fairbydesign.nl/ontology/host_behaviour) |

