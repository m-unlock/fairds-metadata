
# Term: reference database(s)
    
List of database(s) used for ORF annotation, along with version number and reference to website or publication


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:reference_database(s)](http://fairbydesign.nl/ontology/reference_database(s)) |

