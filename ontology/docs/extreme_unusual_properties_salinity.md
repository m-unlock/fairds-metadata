
# Term: extreme_unusual_properties/salinity
    
measured salinity


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 psu`                             |
| Unit            | `psu`                                |
| URL             | [fairds:extreme_unusual_properties_salinity](http://fairbydesign.nl/ontology/extreme_unusual_properties_salinity) |

