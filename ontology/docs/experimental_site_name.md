
# Term: Experimental site name
    
The name of the natural site, experimental field, greenhouse, phenotyping facility, etc. where the experiment took place.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `INRA, UE Diascope - Chemin de Mezouls - Domaine expérimental de Melgueil - 34130 Mauguio – France`                             |
| URL             | [fairds:experimental_site_name](http://fairbydesign.nl/ontology/experimental_site_name) |

