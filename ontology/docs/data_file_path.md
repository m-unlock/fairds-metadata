
# Term: Data File Path
    
Link to the data file (or digital object) in a public database or in a persistent institutional repository; or identifier of the data file when submitted together with the MIAPPE submission.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{file}`                              |
| URL             | [fairds:data_file_path](http://fairbydesign.nl/ontology/data_file_path) |

