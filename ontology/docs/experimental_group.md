
# Term: Experimental Group
    
Specification of experimental groups established in the study


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `E. coli inoculation`                             |
| URL             | [fairds:experimental_group](http://fairbydesign.nl/ontology/experimental_group) |

