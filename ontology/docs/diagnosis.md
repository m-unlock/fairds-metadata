
# Term: diagnosis
    
The investigation, analysis and recognition of the presence and nature of disease, condition, or injury from expressed signs and symptoms; also, the scientific determination of any kind; the concise results of such an investigation (http://ncicb.nci.nih.gov/xml/owl/EVS/Thesaurus.owl#C15220).


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:diagnosis](http://fairbydesign.nl/ontology/diagnosis) |

