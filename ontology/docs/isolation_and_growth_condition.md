
# Term: isolation and growth condition
    
Publication reference in the form of pubmed ID (pmid), digital object identifier (doi) or url for isolation and growth condition specifications of the organism/material. Mandatory for MIGS and MIMARKS Specimen.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `^(doi\:)?\d{2}\.\d{4}.*$`                              |
| URL             | [fairds:isolation_and_growth_condition](http://fairbydesign.nl/ontology/isolation_and_growth_condition) |

