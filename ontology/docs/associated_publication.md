
# Term: Associated publication
    
An identifier for a literature publication where the investigation is described. Use of DOIs is recommended.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{DOI}`                              |
| Example         | `10.1371/journal.pone.0071377`                             |
| URL             | [fairds:associated_publication](http://fairbydesign.nl/ontology/associated_publication) |

