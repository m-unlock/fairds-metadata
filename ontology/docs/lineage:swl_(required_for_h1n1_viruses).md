
# Term: lineage:swl (required for H1N1 viruses)
    
Does the H1N1 influenza virus originate from a swine-like outbreak (as opposed to a seasonal flu)?


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{boolean}`                              |
| Example         | `true`                             |
| URL             | [fairds:lineage:swl_(required_for_h1n1_viruses)](http://fairbydesign.nl/ontology/lineage:swl_(required_for_h1n1_viruses)) |

