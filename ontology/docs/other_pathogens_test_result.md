
# Term: other pathogens test result
    
Classification of a sample as positive or negative based on the test performed and reported. If tests for multiple pathogenic organisms were performed, please state the results in the same order in which you reported the tests in the field 'other pathogens tested'. Format: P(ositive)/N(egative)/not applicable. Example: N


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:other_pathogens_test_result](http://fairbydesign.nl/ontology/other_pathogens_test_result) |

