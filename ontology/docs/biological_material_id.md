
# Term: Biological material ID
    
Code used to identify the biological material in the data file. Should be unique within the Investigation. Can correspond to experimental plant ID, seed lot ID, etc… This material identification is different from a BiosampleID which corresponds to Observation Unit or Samples sections below.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{id}`                              |
| Example         | `ABX_1`                             |
| URL             | [fairds:biological_material_id](http://fairbydesign.nl/ontology/biological_material_id) |

