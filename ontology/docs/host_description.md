
# Term: host description
    
Other descriptive information relating to the host.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| URL             | [fairds:host_description](http://fairbydesign.nl/ontology/host_description) |

