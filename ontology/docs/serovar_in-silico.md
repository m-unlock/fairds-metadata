
# Term: serovar_in-silico
    
serological variety of a species characterized by its antigenic properties derived by in silico prediction


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:serovar_in-silico](http://fairbydesign.nl/ontology/serovar_in-silico) |

