
# Term: organism phenotype
    
most relevant phenotypic traits of the subject; for Phenotypic quality Ontology (PATO) (v 2013-10-28) terms, please see http://purl.bioontology.org/ontology/PATO, e.g. bifurcated (PATO_0001784); terms from Trait Ontology (TO), Plant Ontology (PO) or Crop Ontology (CO) are also accepted; include name/method/scale for each trait; can include multiple traits


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:organism_phenotype](http://fairbydesign.nl/ontology/organism_phenotype) |

