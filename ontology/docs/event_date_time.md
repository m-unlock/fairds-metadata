
# Term: Event Date/Time
    
Date and time in UTC when the sampling event started and ended, e.g. each CTD cast, net tow, or bucket collection is a distinct event. Format: yyyy-mm-ddThh:mm:ssZ. Example: 2013-06-21T14:05:00Z/2013-06-21T14:46:00Z.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{datetime}`                              |
| URL             | [fairds:event_date_time](http://fairbydesign.nl/ontology/event_date_time) |

