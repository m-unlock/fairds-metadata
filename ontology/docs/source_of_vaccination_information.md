
# Term: source of vaccination information
    
Designation of information related to vaccination history as self reported or documented.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(documented|self reported)`                              |
| Example         | `documented`                             |
| URL             | [fairds:source_of_vaccination_information](http://fairbydesign.nl/ontology/source_of_vaccination_information) |

