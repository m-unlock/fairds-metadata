
# Term: Salinity Sensor
    
Salinity of water at the time of taking the sample. Format: ##.####, SDN:P02:75:PSAL, SDN:P06:46:UGKG for PSU. Example: 39.2268.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `39.2268 psu`                             |
| Unit            | `psu`                                |
| URL             | [fairds:salinity_sensor](http://fairbydesign.nl/ontology/salinity_sensor) |

