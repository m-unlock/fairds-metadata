
# Term: surface air contaminant
    
contaminant identified on surface


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(biocides|biological contaminants|dust|nutrients|organic matter|particulate matter|radon|volatile organic compounds)`                              |
| Example         | `biological contaminants`                             |
| URL             | [fairds:surface_air_contaminant](http://fairbydesign.nl/ontology/surface_air_contaminant) |

