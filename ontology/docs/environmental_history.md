
# Term: environmental history
    
Information concerning the envinonrment a material entity has been exposed to, such as an organism from a lake.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:environmental_history](http://fairbydesign.nl/ontology/environmental_history) |

