
# Term: Material source DOI
    
Digital Object Identifier (DOI) of the material source


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{DOI}`                              |
| Example         | `10.15454/1.4658436467893904E12`                             |
| URL             | [fairds:material_source_doi](http://fairbydesign.nl/ontology/material_source_doi) |

