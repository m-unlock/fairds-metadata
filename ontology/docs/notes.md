
# Term: notes
    
General notes on a given sample, assay, etc..


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| URL             | [fairds:notes](http://fairbydesign.nl/ontology/notes) |

