
# Term: Health Score
    
Health condition: 1=Normal, 2=Depressed/sad, 3=Very much affected


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(1|2|3)`                              |
| Example         | `2`                             |
| URL             | [fairds:health_score](http://fairbydesign.nl/ontology/health_score) |

