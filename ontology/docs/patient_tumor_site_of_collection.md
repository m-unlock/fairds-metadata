
# Term: patient tumor site of collection
    
Site of collection of the patient tissue sample which was extracted (can be different to primary site if it is a metastatic sample). Please use NCIT ontology, e.g. Liver (http://purl.obolibrary.org/obo/NCIT_C12392). If unknown please select NCIT term: ‚ÄúNot Available‚Äù (http://purl.obolibrary.org/obo/NCIT_C126101)


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:patient_tumor_site_of_collection](http://fairbydesign.nl/ontology/patient_tumor_site_of_collection) |

