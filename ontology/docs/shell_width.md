
# Term: shell width
    
width of shell (perpendicular angle to length)


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 g`                             |
| Unit            | `g`                                |
| URL             | [fairds:shell_width](http://fairbydesign.nl/ontology/shell_width) |

