
# Term: disease staging
    
The stage or progression of a disease in an organism. Includes pathological staging of cancers and other disease progression. E.g. Dukes C stage describing colon cancer (EFO_0000410).


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:disease_staging](http://fairbydesign.nl/ontology/disease_staging) |

