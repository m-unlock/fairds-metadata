
# Term: Sequencing Kit
    
Kit used for the preparation of the sample


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `Kit 12/Q20+, Ultra-Long`                             |
| URL             | [fairds:sequencing_kit](http://fairbydesign.nl/ontology/sequencing_kit) |

