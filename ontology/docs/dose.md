
# Term: dose
    
The total quantity or strength of a substance administered at one time.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:dose](http://fairbydesign.nl/ontology/dose) |

