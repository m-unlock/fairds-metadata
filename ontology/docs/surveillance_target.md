
# Term: surveillance target
    
Valid species level NCBI taxon id of the organism being surveyed for (if any) e.g. for Escherichia coli enter 562.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:surveillance_target](http://fairbydesign.nl/ontology/surveillance_target) |

