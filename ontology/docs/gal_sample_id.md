
# Term: GAL_sample_id
    
unique name assigned to the sample by the genome acquisition lab.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:gal_sample_id](http://fairbydesign.nl/ontology/gal_sample_id) |

