
# Term: influenza strain unique number
    
Unique number of the strain which is reported as a part of the influenza strain name, such as A/chicken/Fujian/411/2002(HxN1). Format: integer number, Example: 411


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `411`                             |
| URL             | [fairds:influenza_strain_unique_number](http://fairbydesign.nl/ontology/influenza_strain_unique_number) |

