
# Term: read quality filter
    
Programme used to filter reads quality before conducting the analysis


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:read_quality_filter](http://fairbydesign.nl/ontology/read_quality_filter) |

