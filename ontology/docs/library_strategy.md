
# Term: library strategy
    
Source ENA


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(WGS|WGA|WXS|RNA-Seq|ssRNA-seq|miRNA-Seq|ncRNA-Seq|FL-cDNA|EST|Hi-C|ATAC-seq|WCS|RAD-Seq|CLONE|POOLCLONE|AMPLICON|CLONEEND|FINISHING|ChIP-Seq|MNase-Seq|DNase-Hypersensitivity|Bisulfite-Seq|CTS|MRE-Seq|MeDIP-Seq|MBD-Seq|Tn-Seq|VALIDATION|FAIRE-seq|SELEX|RIP-Seq|ChIA-PET|Synthetic-Long-Read|Targeted-Capture|Tethered_Chromatin_Conformation_Capture|OTHER)`                              |
| Example         | `AMPLICON`                             |
| URL             | [fairds:library_strategy](http://fairbydesign.nl/ontology/library_strategy) |

