
# Term: WGA amplification approach
    
Method used to amplify genomic DNA in preparation for sequencing


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(mda based|pcr based)`                              |
| URL             | [fairds:wga_amplification_approach](http://fairbydesign.nl/ontology/wga_amplification_approach) |

