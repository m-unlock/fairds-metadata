
# Term: variety
    
variety (= varietas, a formal Linnaean rank) of organism from which sample was derived.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:variety](http://fairbydesign.nl/ontology/variety) |

