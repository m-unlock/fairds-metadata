
# Term: meaning of cut off value
    
Description helping to explain what the cut off value means.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:meaning_of_cut_off_value](http://fairbydesign.nl/ontology/meaning_of_cut_off_value) |

