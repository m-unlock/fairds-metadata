
# Term: Biological material preprocessing
    
Description of any process or treatment applied uniformly to the biological material, prior to the study itself. Can be provided as free text or as an accession number from a suitable controlled vocabulary.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `EO:0007210 – PVY(NTN) ; transplanted from study http://phenome-fppn.fr/maugio/2013/t2351 ; observation unit ID: pot:894`                             |
| URL             | [fairds:biological_material_preprocessing](http://fairbydesign.nl/ontology/biological_material_preprocessing) |

