
# Term: subspecific genetic lineage name
    
name of the infraspecific rank, e.g ecotype Col-0


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:subspecific_genetic_lineage_name](http://fairbydesign.nl/ontology/subspecific_genetic_lineage_name) |

