
# Term: Sampling Station
    
Refers to the site/station where data/sample collection is performed. Example: TARA_100.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sampling_station](http://fairbydesign.nl/ontology/sampling_station) |

