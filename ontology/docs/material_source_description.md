
# Term: Material source description
    
Description of the material source


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `Branches were collected from a 10-year-old tree growing in a progeny trial established in a loamy brown earth soil.`                             |
| URL             | [fairds:material_source_description](http://fairbydesign.nl/ontology/material_source_description) |

