
# Term: diagnostic method
    
The method or procedure (imaging, genetic or other) performed to determine or confirm the presence of disease in an individual. Examples: microscopy, immunofluorescence, PCR, rapid tests, etc.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:diagnostic_method](http://fairbydesign.nl/ontology/diagnostic_method) |

