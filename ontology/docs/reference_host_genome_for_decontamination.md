
# Term: reference host genome for decontamination
    
Reference host genome that was mapped against for host decontamination (in the form of a valid assembly accession - e.g. in the format GCA_xxxxxxx.x)


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(^(E|D|S)RZ[0-9]{6,}$)|(^GC(A|F)_[0-9]{9}.[0-9]+$|^[A-Z]{1}[0-9]{5}.[0-9]+$|^[A-Z]{2}[0-9]{6}.[0-9]+$|^[A-Z]{2}[0-9]{8}$|^[A-Z]{4}[0-9]{2}S?[0-9]{6,8}$|^[A-Z]{6}[0-9]{2}S?[0-9]{7,9}$)`                              |
| Example         | `GCA_000001405.29`                             |
| URL             | [fairds:reference_host_genome_for_decontamination](http://fairbydesign.nl/ontology/reference_host_genome_for_decontamination) |

