
# Term: Was the PDX model humanised?
    
"If the sample origin is ‚Äúengrafted tumor‚Äù, please indicate if the host strain, from which the sample was extracted, has undergone human immune system reconstitution, using ‚ÄúYes‚Äù (model humanised) or ‚ÄúNo‚Äù (model not humanised). If the sample origin is ‚Äúpatient tumor‚Äù please select ""not applicable""."


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{boolean}`                              |
| Example         | `false`                             |
| URL             | [fairds:was_the_pdx_model_humanised?](http://fairbydesign.nl/ontology/was_the_pdx_model_humanised?) |

