
# Term: Body Weight
    
Individual body weight at sampling


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `6000g`                             |
| Unit            | `g`                                |
| URL             | [fairds:body_weight](http://fairbydesign.nl/ontology/body_weight) |

