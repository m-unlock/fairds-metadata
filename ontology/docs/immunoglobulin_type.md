
# Term: Immunoglobulin Type
    
Type of immunoglobulin measured: IgG (Immunoglobulin G), IgA (Immunoglobulin A), IgM (Immunoglobulin M)


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(IgG|IgA|IgM)`                              |
| Example         | `IgA`                             |
| URL             | [fairds:immunoglobulin_type](http://fairbydesign.nl/ontology/immunoglobulin_type) |

