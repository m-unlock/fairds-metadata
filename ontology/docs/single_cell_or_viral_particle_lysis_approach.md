
# Term: single cell or viral particle lysis approach
    
Method used to free DNA from interior of the cell(s) or particle(s)


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(chemical|combination|enzymatic|physical)`                              |
| Example         | `chemical`                             |
| URL             | [fairds:single_cell_or_viral_particle_lysis_approach](http://fairbydesign.nl/ontology/single_cell_or_viral_particle_lysis_approach) |

