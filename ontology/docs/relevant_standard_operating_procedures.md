
# Term: relevant standard operating procedures
    
Standard operating procedures used in assembly and/or annotation of genomes, metagenomes or environmental sequences


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:relevant_standard_operating_procedures](http://fairbydesign.nl/ontology/relevant_standard_operating_procedures) |

