
# Term: subject exposure
    
Exposure of the subject to infected human or animals, such as poultry, wild bird or swine. If multiple exposures are applicable, please state them separated by semicolon. Example: poultry; wild bird


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:subject_exposure](http://fairbydesign.nl/ontology/subject_exposure) |

