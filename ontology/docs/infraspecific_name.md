
# Term: Infraspecific name
    
"Name of any subtaxa level, including variety, crossing name, etc. It can be used to store any additional taxonomic identifier. To be filled as key-value pair list format (the key is the name of the rank/category and the value is the value of  the rank/category). Ranks/categories can be among the following terms: subspecies, cultivar, variety, subvariety, convariety, group, subgroup, hybrid, line, form, subform. For MCPD compliance, the following abbreviations are allowed: ""subsp."" (subspecies); ""convar."" (convariety); ""var."" (variety); ""f."" (form); ""Group"" (cultivar group). MIAPPE adds ""cv."" (cultivar)."


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `subspecies:vinifera, cultivar:Pinot noir ; subsp.:aestivum, cv.:Weneda, Group:winter ; subsp. vinifera cv. Pinot Noir`                             |
| URL             | [fairds:infraspecific_name](http://fairbydesign.nl/ontology/infraspecific_name) |

