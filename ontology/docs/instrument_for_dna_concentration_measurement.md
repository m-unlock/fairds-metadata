
# Term: instrument for DNA concentration measurement
    
Name/type of instrument used to measure the DNA concentration (ng/µl) prior to library preparation


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:instrument_for_dna_concentration_measurement](http://fairbydesign.nl/ontology/instrument_for_dna_concentration_measurement) |

