
# Term: library size
    
Total number of clones in the library prepared for the project


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5`                             |
| URL             | [fairds:library_size](http://fairbydesign.nl/ontology/library_size) |

