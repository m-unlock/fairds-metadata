
# Term: Trait Characteristic
    
Characteristic measured. It can be a morphological characteristic (size, volume, surface), a molecular characteristic (sugar concentration), etc...


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `Area`                             |
| URL             | [fairds:trait_characteristic](http://fairbydesign.nl/ontology/trait_characteristic) |

