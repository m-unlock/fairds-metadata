
# Term: vaccine lot number
    
Lot number of the vaccine.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `LOT12345`                             |
| URL             | [fairds:vaccine_lot_number](http://fairbydesign.nl/ontology/vaccine_lot_number) |

