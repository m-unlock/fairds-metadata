
# Term: Fecal Score (Lab)
    
Lab-measured fecal consistency from rectal samples: 1=Hard/dry/tuberous, 2=Firm, 3=Soft but malleable, 4=Soft and fluid, 5=Watery dark, 6=Watery yellow, 7=Yellow, foamy


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(1|2|3|4|5|6|7)`                              |
| Example         | `4`                             |
| URL             | [fairds:fecal_score_(lab)](http://fairbydesign.nl/ontology/fecal_score_(lab)) |

