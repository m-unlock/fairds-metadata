
# Term: size of the catchment area
    
Refers to the size of the area that is drained by the sampled sewage system in square km.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:size_of_the_catchment_area](http://fairbydesign.nl/ontology/size_of_the_catchment_area) |

