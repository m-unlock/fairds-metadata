
# Term: phenotype
    
Where possible, please use the Experimental Factor Ontology (EFO) to describe your phenotypes.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:phenotype](http://fairbydesign.nl/ontology/phenotype) |

