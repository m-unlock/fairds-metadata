
# Term: tumor grading (OBI_0600002)
    
Determination of the grade (severity/stage) of a tumor sample, used in cancer biology to describe abnormalities/qualities of tumor cells or tissues. Values can be described by terms from NCI Thesaurus.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:tumor_grading_(obi_0600002)](http://fairbydesign.nl/ontology/tumor_grading_(obi_0600002)) |

