
# Term: Biological material altitude
    
Altitude of the studied biological material, provided in meters (m). [Alternative identifier for in situ material]


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `10 m`                             |
| Unit            | `m`                                |
| URL             | [fairds:biological_material_altitude](http://fairbydesign.nl/ontology/biological_material_altitude) |

