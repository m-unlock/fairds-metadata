
# Term: Blood sample site
    
Location from which blood is collected


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(Cranial vena cava|Marginal ear veins|External jugular vein)`                              |
| Example         | `Cranial vena cava`                             |
| URL             | [fairds:blood_sample_site](http://fairbydesign.nl/ontology/blood_sample_site) |

