
# Term: host prediction estimated accuracy
    
For each tool or approach used for host prediction, estimated false discovery rates should be included, either computed de novo or from the literature


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:host_prediction_estimated_accuracy](http://fairbydesign.nl/ontology/host_prediction_estimated_accuracy) |

