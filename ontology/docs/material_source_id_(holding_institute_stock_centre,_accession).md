
# Term: Material source ID (Holding institute/stock centre, accession)
    
"An identifier for the source of the biological material, in the form of a key-value pair comprising the name/identifier of the repository from which the material was sourced plus the accession number of the repository for that material. Where an accession number has not been assigned, but the material has been derived from the crossing of known accessions, the material can be defined as follows: ""mother_accession X father_accession"", or, if father is unknown, as ""mother_accession X UNKNOWN"". For in situ material, the region of provenance may be used when an accession is not available. The Material source is commonly called germplasm, accession, genotype and even variety for commercial varieties. For the latest, keep in mind that a variety is commonly ambiguously identified and polysemous"


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `INRA:W95115_inra ; ICNF:PNB-RPI`                             |
| URL             | [fairds:material_source_id_(holding_institute_stock_centre,_accession)](http://fairbydesign.nl/ontology/material_source_id_(holding_institute_stock_centre,_accession)) |

