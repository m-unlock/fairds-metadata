
# Term: Site for Histology
    
Sampling locations: SI-10: 10% of SI length, SI-50: 50% of SI length, SI-90: 90% of SI length, CO-10: 10% of colon length, CO-50: 50% of colon length, CO-90: 90% of colon length


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(SI-10|SI-50|SI-90|CO-10|CO-50|CO-90)`                              |
| Example         | `SI-10`                             |
| URL             | [fairds:site_for_histology](http://fairbydesign.nl/ontology/site_for_histology) |

