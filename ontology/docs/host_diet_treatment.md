
# Term: host diet treatment
    
Experimental diet treatment/additive to normal host feed e.g. Probiotic (GalliPro EPB5)


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| URL             | [fairds:host_diet_treatment](http://fairbydesign.nl/ontology/host_diet_treatment) |

