
# Term: Pig breed
    
Y = Yorkshire, L = Landrace, D = Duroc, YLD = 25%Y×25%L×50%D, YL = 50%Y×50%L, TN = Topigs Norsvin


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(YLD|YL|Y|L|D|TN)`                              |
| Example         | `YLD`                             |
| URL             | [fairds:pig_breed](http://fairbydesign.nl/ontology/pig_breed) |

