
# Term: source material description
    
further information to clarify the nature of the specimen or population used that is not collected elsewhere, e.g. if source was derived from accessioned stock, describe how it links to the original material


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:source_material_description](http://fairbydesign.nl/ontology/source_material_description) |

