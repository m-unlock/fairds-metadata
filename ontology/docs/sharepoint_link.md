
# Term: sharepoint link
    
Link to the sharepoint environment used in this investigation


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{url}`                              |
| URL             | [fairds:sharepoint_link](http://fairbydesign.nl/ontology/sharepoint_link) |

