
# Term: taxonomic classification
    
Method used for taxonomic classification, along with reference database used, classification rank, and thresholds used to classify new genomes. Expected values are: classification method, database name, and other parameters e.g. vConTACT vContact2 (references from NCBI RefSeq v83, genus rank classification, default parameters)


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:taxonomic_classification](http://fairbydesign.nl/ontology/taxonomic_classification) |

