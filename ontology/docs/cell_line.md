
# Term: cell_line
    
cell line from which the sample was obtained


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:cell_line](http://fairbydesign.nl/ontology/cell_line) |

