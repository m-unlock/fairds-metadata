
# Term: Treatment Type
    
Name of antibiotic or other treatments


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `400 ppm colistin and 100 ppm kitasamycin in feed`                             |
| URL             | [fairds:treatment_type](http://fairbydesign.nl/ontology/treatment_type) |

