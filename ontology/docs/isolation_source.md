
# Term: isolation source
    
describes the physical, environmental and/or local geographical source of the biological sample from which the sample was derived


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:isolation_source](http://fairbydesign.nl/ontology/isolation_source) |

