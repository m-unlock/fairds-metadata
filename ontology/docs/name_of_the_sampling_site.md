
# Term: name of the sampling site
    
Refers to the name of the site/station where data/sample collection is performed.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:name_of_the_sampling_site](http://fairbydesign.nl/ontology/name_of_the_sampling_site) |

