
# Term: Sample Status
    
Refers to the completness of sample metadata validation. Example: preliminary but can be used to provide data discovery services.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sample_status](http://fairbydesign.nl/ontology/sample_status) |

