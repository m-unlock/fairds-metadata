
# Term: organism part
    
The part of organism's anatomy or substance arising from an organism from which the biomaterial was derived, excludes cells.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:organism_part](http://fairbydesign.nl/ontology/organism_part) |

