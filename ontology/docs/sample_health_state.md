
# Term: sample health state
    
health status of the subject at the time of sample collection


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(diseased|healthy)`                              |
| Example         | `diseased`                             |
| URL             | [fairds:sample_health_state](http://fairbydesign.nl/ontology/sample_health_state) |

