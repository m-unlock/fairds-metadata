
# Term: host breed
    
Breed of the host (e.g. COBB-500)


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:host_breed](http://fairbydesign.nl/ontology/host_breed) |

