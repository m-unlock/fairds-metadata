
# Term: strain
    
Name of the strain from which the sample was obtained.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:strain](http://fairbydesign.nl/ontology/strain) |

