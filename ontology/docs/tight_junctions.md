
# Term: Tight Junctions
    
Type of tight junction protein analyzed


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(ZO-1|ZO-2|ZO-3|Occludin|Claudin)`                              |
| Example         | `Occludin`                             |
| URL             | [fairds:tight_junctions](http://fairbydesign.nl/ontology/tight_junctions) |

