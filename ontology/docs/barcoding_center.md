
# Term: barcoding center
    
Center where DNA barcoding was/will be performed.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:barcoding_center](http://fairbydesign.nl/ontology/barcoding_center) |

