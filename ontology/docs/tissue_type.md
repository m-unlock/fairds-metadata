
# Term: tissue_type
    
tissue type from which the sample was obtained


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:tissue_type](http://fairbydesign.nl/ontology/tissue_type) |

