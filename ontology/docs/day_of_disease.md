
# Term: Day of disease
    
Date at which the disease is noticed


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `2023-11-06 00:00:00`                             |
| URL             | [fairds:day_of_disease](http://fairbydesign.nl/ontology/day_of_disease) |

