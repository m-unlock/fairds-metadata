
# Term: host health state
    
Health status of the host at the time of sample collection.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(diseased|healthy|not applicable|not collected|not provided|restricted access)`                              |
| Example         | `healthy`                             |
| URL             | [fairds:host_health_state](http://fairbydesign.nl/ontology/host_health_state) |

