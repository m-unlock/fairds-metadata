
# Term: Gastrointestinal Tract Segment
    
STO: stomach, SI: total small intestine, SI-1: first 1/3 of small intestine, SI-2: second 1/3 of small intestine, SI-3: third 1/3 of small intestine, CAE: caecum, CO: total colon, CO-1: first 1/3 of colon, CO-2: second 1/3 of colon, CO-3: third 1/3 of colon


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(STO|SI|SI-1|SI-2|SI-3|CAE|CO|CO-1|CO-2|CO-3)`                              |
| Example         | `STO`                             |
| URL             | [fairds:gastrointestinal_tract_segment](http://fairbydesign.nl/ontology/gastrointestinal_tract_segment) |

