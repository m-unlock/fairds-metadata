
# Term: Material source altitude
    
Altitude of the material source, provided in metres (m). [Alternative identifier for in situ material]


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `10 m`                             |
| Unit            | `m`                                |
| URL             | [fairds:material_source_altitude](http://fairbydesign.nl/ontology/material_source_altitude) |

