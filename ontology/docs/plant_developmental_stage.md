
# Term: plant developmental stage
    
developmental stage at the time of sample collection; for Plant Ontology (PO) (v 20) terms, see http://purl.bioontology.org/ontology/PO, e.g. hypocotyl emergence stage (PO_0007043)


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:plant_developmental_stage](http://fairbydesign.nl/ontology/plant_developmental_stage) |

