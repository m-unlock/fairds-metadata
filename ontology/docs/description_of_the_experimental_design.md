
# Term: Description of the experimental design
    
"Short description of the experimental design, possibly including statistical design. In specific cases, e.g. legacy datasets or data computed from several studies, the experimental design can be ""unknown""/""NA"", ""aggregated/reduced data"", or simply ""none""."


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `Lines were repeated twice at each location using a complete block design. In order to limit competition effects, each block was organized into four sub-blocks corresponding to earliness groups based on a priori information.`                             |
| URL             | [fairds:description_of_the_experimental_design](http://fairbydesign.nl/ontology/description_of_the_experimental_design) |

