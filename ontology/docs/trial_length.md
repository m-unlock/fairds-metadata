
# Term: trial length
    
Length of time from the beginning of the trial until the end of the trial


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 days`                             |
| Unit            | `days|hours|minutes|weeks|years`                                |
| URL             | [fairds:trial_length](http://fairbydesign.nl/ontology/trial_length) |

