
# Term: Material source longitude
    
Longitude of the material source. [Alternative identifier for in situ material]


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(^{number}.?[0-9]{0,8}$)|(^not collected$)|(^not provided$)|(^restricted access$)`                              |
| Example         | `-8.73`                             |
| URL             | [fairds:material_source_longitude](http://fairbydesign.nl/ontology/material_source_longitude) |

