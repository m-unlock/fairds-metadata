
# Term: storage conditions
    
explain how and for how long the soil sample was stored before DNA extraction.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(fresh|frozen|other)`                              |
| Example         | `frozen`                             |
| URL             | [fairds:storage_conditions](http://fairbydesign.nl/ontology/storage_conditions) |

