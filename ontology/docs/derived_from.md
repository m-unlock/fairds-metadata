
# Term: derived from
    
When a sample is taken from a sample, the identifier of that sample should  be given here.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `Sample31WintersWijk`                             |
| URL             | [fairds:derived_from](http://fairbydesign.nl/ontology/derived_from) |

