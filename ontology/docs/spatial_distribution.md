
# Term: Spatial distribution
    
Type and value of a spatial coordinate (georeference or relative) or level of observation (plot 45, subblock 7, block 2) provided as a key-value pair of the form type:value. Levels of observation must be consistent with those listed in the Study section.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `latitude:+2.341, row:4, X:3, Y:6, Xm:35, Ym:65; block:1; plot:894`                             |
| URL             | [fairds:spatial_distribution](http://fairbydesign.nl/ontology/spatial_distribution) |

