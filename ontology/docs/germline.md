
# Term: germline
    
the sample described presented in the entry has not undergone somatic genomic rearrangement as part of an adaptive immune response; it is the unrearranged molecule that was inherited from the parental germline


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:germline](http://fairbydesign.nl/ontology/germline) |

