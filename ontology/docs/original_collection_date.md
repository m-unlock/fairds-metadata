
# Term: original collection date
    
For use if the specimen is from a zoo, botanic garden, culture collection etc. and has a known original date of collection. In case no exact time is available, the date/time can be right truncated i.e. all of these are valid ISO8601 compliant times: 2008-01-23T19:23:10+00:00; 2008-01-23T19:23:10; 2008-01-23; 2008-01; 2008.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `41406`                             |
| URL             | [fairds:original_collection_date](http://fairbydesign.nl/ontology/original_collection_date) |

