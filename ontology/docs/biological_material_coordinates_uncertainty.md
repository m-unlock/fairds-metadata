
# Term: Biological material coordinates uncertainty
    
Circular uncertainty of the coordinates, preferably provided in meters (m). [Alternative identifier for in situ material]


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `200 m`                             |
| Unit            | `m`                                |
| URL             | [fairds:biological_material_coordinates_uncertainty](http://fairbydesign.nl/ontology/biological_material_coordinates_uncertainty) |

