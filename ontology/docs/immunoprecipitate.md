
# Term: immunoprecipitate
    
The precipitate antibody bound target molecules generated when precipitating an antigen out of a solution during the process of immunoprecipitation.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:immunoprecipitate](http://fairbydesign.nl/ontology/immunoprecipitate) |

