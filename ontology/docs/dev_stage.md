
# Term: dev_stage
    
if the sample was obtained from an organism in a specific developmental stage, it is specified with this qualifier


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:dev_stage](http://fairbydesign.nl/ontology/dev_stage) |

