
# Term: toxin burden
    
Concentration of toxins in the organism at the time of sampling


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 g`                             |
| Unit            | `g|kg|mg`                                |
| URL             | [fairds:toxin_burden](http://fairbydesign.nl/ontology/toxin_burden) |

