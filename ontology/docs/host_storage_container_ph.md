
# Term: host storage container pH
    
pH of the medium in which the host organism is kept before sampling (e.g. pH of water in tank)


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `6`                             |
| URL             | [fairds:host_storage_container_ph](http://fairbydesign.nl/ontology/host_storage_container_ph) |

