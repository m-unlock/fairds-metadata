
# Term: collected by
    
name of persons or institute who collected the specimen


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `Jane Doe`                             |
| URL             | [fairds:collected_by](http://fairbydesign.nl/ontology/collected_by) |

