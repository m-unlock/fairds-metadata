
# Term: collecting institution
    
Name of the institution to which the person collecting the specimen belongs. Format: Institute Name, Institute Address


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:collecting_institution](http://fairbydesign.nl/ontology/collecting_institution) |

