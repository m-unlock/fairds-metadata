
# Term: Trait Entity
    
Entity (part of the plant, whole plant, group of plant e.g. canopy) on which the trait has been measured


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| Example         | `Leaf`                             |
| URL             | [fairds:trait_entity](http://fairbydesign.nl/ontology/trait_entity) |

