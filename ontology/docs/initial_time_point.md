
# Term: initial time point
    
The first time point measured at the start of some process.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{timestamp}`                              |
| Example         | `2013-03-25T12:42:31`                             |
| URL             | [fairds:initial_time_point](http://fairbydesign.nl/ontology/initial_time_point) |

