
# Term: pcr primers
    
PCR primers that were used to amplify the sequence of the targeted gene, locus or subfragment. This field should contain all the primers used for a single PCR reaction if multiple forward or reverse primers are present in a single PCR reaction. The primer sequence should be reported in uppercase letters


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:pcr_primers](http://fairbydesign.nl/ontology/pcr_primers) |

