
# Term: single cell or viral particle lysis kit protocol
    
Name of the kit or standard protocol used for cell(s) or particle(s) lysis


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:single_cell_or_viral_particle_lysis_kit_protocol](http://fairbydesign.nl/ontology/single_cell_or_viral_particle_lysis_kit_protocol) |

