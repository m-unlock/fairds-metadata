
# Term: library reads sequenced
    
Total number of clones sequenced from the library


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `32978456`                             |
| URL             | [fairds:library_reads_sequenced](http://fairbydesign.nl/ontology/library_reads_sequenced) |

