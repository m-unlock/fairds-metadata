
# Term: area of sampling site
    
Please indicate if there are specific facilities in the area covered by the sewage sample. For example: farming, slaughterhouse(s), industry, hospital(s) or any other facility.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:area_of_sampling_site](http://fairbydesign.nl/ontology/area_of_sampling_site) |

