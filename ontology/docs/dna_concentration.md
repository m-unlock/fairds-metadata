
# Term: DNA concentration
    
DNA concentration used for sequencing in ng/µl


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `100 ng/µl`                             |
| Unit            | `ng/µl`                                |
| URL             | [fairds:dna_concentration](http://fairbydesign.nl/ontology/dna_concentration) |

