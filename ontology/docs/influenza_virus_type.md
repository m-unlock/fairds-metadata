
# Term: influenza virus type
    
One of the three influenza virus classification types.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(A|B|C)`                              |
| Example         | `B`                             |
| URL             | [fairds:influenza_virus_type](http://fairbydesign.nl/ontology/influenza_virus_type) |

