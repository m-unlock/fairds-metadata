
# Term: trial timepoint
    
Timepoint of the trial when the sample was collected - length of time after the beginning of the trial


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `5 days`                             |
| Unit            | `days|hours|minutes|weeks|years`                                |
| URL             | [fairds:trial_timepoint](http://fairbydesign.nl/ontology/trial_timepoint) |

