
# Term: Contact institution
    
Name and address of the institution responsible for the study.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{text}`                              |
| URL             | [fairds:contact_institution](http://fairbydesign.nl/ontology/contact_institution) |

