
# Term: environmental medium
    
In this field, report which environmental material or materials (pipe separated) immediately surrounded your sample or specimen prior to sampling. EnvO terms can be found via the link: https://www.ebi.ac.uk/ols4


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:environmental_medium](http://fairbydesign.nl/ontology/environmental_medium) |

