
# Term: Trait Entity Accession number
    
Accession number of the trait entity in a suitable controlled vocabulary (Plant Ontology).


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `http://purl.obolibrary.org/obo/PO_0025034`                             |
| URL             | [fairds:trait_entity_accession_number](http://fairbydesign.nl/ontology/trait_entity_accession_number) |

