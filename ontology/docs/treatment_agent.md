
# Term: treatment agent
    
The name of the treatment agent used.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:treatment_agent](http://fairbydesign.nl/ontology/treatment_agent) |

