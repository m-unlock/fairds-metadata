
# Term: Map of experimental design
    
Representation of the experimental design.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `https://urgi.versailles.inra.fr/files/ephesis/181000503/181000503_plan.xls`                             |
| URL             | [fairds:map_of_experimental_design](http://fairbydesign.nl/ontology/map_of_experimental_design) |

