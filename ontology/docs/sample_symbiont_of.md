
# Term: sample symbiont of
    
Reference to host sample from symbiont. The referenced sample should already be registered in INSDC. E.g. ERSxxxxxx


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(^[ESD]RS\d{6,}$)|(^SAM[END][AG]?\d+$)|(^EGAN\d{11}$)`                              |
| URL             | [fairds:sample_symbiont_of](http://fairbydesign.nl/ontology/sample_symbiont_of) |

