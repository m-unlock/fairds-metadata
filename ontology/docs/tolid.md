
# Term: tolid
    
A ToLID (Tree of Life ID) is a unique and easy to communicate sample identifier that provides species recognition, differentiates between specimen of the same species and adds taxonomic context. ToLIDs are issued by id.tol.sanger.ac.uk. They are endorsed by the EarthBioGenome Project (EBP) and should be assigned to any sample with association to the EBP.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:tolid](http://fairbydesign.nl/ontology/tolid) |

