
# Term: Further Details
    
Reference details related to a sample in form of an URI.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:further_details](http://fairbydesign.nl/ontology/further_details) |

