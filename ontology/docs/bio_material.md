
# Term: bio_material
    
identifier for the biological material from which the sample was obtained, with optional institution code and collection code for the place where it is currently stored.


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:bio_material](http://fairbydesign.nl/ontology/bio_material) |

