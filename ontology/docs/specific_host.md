
# Term: specific host
    
If there is a host involved, please provide its taxid (or environmental if not actually isolated from the dead or alive host - i.e. pathogen could be isolated from a swipe of a bench etc) and report whether it is a laboratory or natural host). From this we can calculate any number of groupings of hosts (e.g. animal vs plant, all fish hosts, etc)


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:specific_host](http://fairbydesign.nl/ontology/specific_host) |

