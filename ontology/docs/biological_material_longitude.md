
# Term: Biological material longitude
    
Longitude of the studied biological material. [Alternative identifier for in situ material]


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(^{number}.?[0-9]{0,8}$)|(^not collected$)|(^not provided$)|(^restricted access$)`                              |
| Example         | `-8.73`                             |
| URL             | [fairds:biological_material_longitude](http://fairbydesign.nl/ontology/biological_material_longitude) |

