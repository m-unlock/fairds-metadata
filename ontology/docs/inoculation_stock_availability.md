
# Term: inoculation stock availability
    
Is the virus stock used for the inoculation available?


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{boolean}`                              |
| Example         | `true`                             |
| URL             | [fairds:inoculation_stock_availability](http://fairbydesign.nl/ontology/inoculation_stock_availability) |

