
# Term: date of birth
    
Date of birth of subject the sample was derived from.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `41358`                             |
| URL             | [fairds:date_of_birth](http://fairbydesign.nl/ontology/date_of_birth) |

