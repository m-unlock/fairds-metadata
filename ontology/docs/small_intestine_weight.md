
# Term: Small Intestine Weight
    
Weight of empty small intestine


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{number}`                              |
| Example         | `3000g`                             |
| Unit            | `g`                                |
| URL             | [fairds:small_intestine_weight](http://fairbydesign.nl/ontology/small_intestine_weight) |

