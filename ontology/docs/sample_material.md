
# Term: sample material
    
"Sample material from which data deposited was generated, e.g. tissue fragment. If unknown please select ""not provided""."


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(bone marrow aspirate|cell suspension|malignant effusion|not provided|organoid|peripheral blood|tissue fragment)`                              |
| Example         | `cell suspension`                             |
| URL             | [fairds:sample_material](http://fairbydesign.nl/ontology/sample_material) |

