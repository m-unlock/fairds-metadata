
# Term: similarity search method
    
Tool used to compare ORFs with database, along with version and cutoffs used. Add names and versions of software(s), parameters used


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:similarity_search_method](http://fairbydesign.nl/ontology/similarity_search_method) |

