
# Term: Enrichment
    
Whether enrichment is provided (rope, chains, hay, wooden blocks, etc.)


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{boolean}`                              |
| Example         | `yes`                             |
| URL             | [fairds:enrichment](http://fairbydesign.nl/ontology/enrichment) |

