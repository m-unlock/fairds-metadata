
# Term: Last Update Date
    
Date of the last update of this sample in the SAMPLES REGISTRY. Example: 2014-03-01Z.


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{date}`                              |
| Example         | `41358`                             |
| URL             | [fairds:last_update_date](http://fairbydesign.nl/ontology/last_update_date) |

