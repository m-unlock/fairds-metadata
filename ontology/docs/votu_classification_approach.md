
# Term: vOTU classification approach
    
Cutoffs and approach used when clustering new UViGs in ‚Äúspecies-level‚Äù vOTUs. Note that results from standard 95% ANI / 85% AF clustering should be provided alongside vOTUS defined from another set of thresholds, even if the latter are the ones primarily used during the analysis. This should be formatted {ANI cutoff};{AF cutoff};{clustering method} e.g. 95% ANI;85% AF; greedy incremental clustering


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:votu_classification_approach](http://fairbydesign.nl/ontology/votu_classification_approach) |

