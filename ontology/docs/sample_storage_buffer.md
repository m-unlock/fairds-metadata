
# Term: sample storage buffer
    
buffer used for sample storage (e.g. RNAlater)


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:sample_storage_buffer](http://fairbydesign.nl/ontology/sample_storage_buffer) |

