
# Term: Observation unit external ID
    
Identifier for the observation unit in a persistent repository, comprises the name of the repository and the identifier of the observation unit therein. The EBI Biosamples repository can be used. URI are recommended when possible.


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `Biosamples:SAMEA4202911`                             |
| URL             | [fairds:observation_unit_external_id](http://fairbydesign.nl/ontology/observation_unit_external_id) |

