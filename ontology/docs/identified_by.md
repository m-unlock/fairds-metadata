
# Term: identified_by
    
name of the expert who identified the specimen taxonomically


|              |                                    |
|------------------|-----------------------------------------|
| Example         | `Jane Doe`                             |
| URL             | [fairds:identified_by](http://fairbydesign.nl/ontology/identified_by) |

