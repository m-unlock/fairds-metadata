
# Term: health or disease status of specific host
    
Health or disease status of specific host at time of collection. This field accepts PATO (v 2013-10-28) terms, for a browser please see http://purl.bioontology.org/ontology/PATO


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:health_or_disease_status_of_specific_host](http://fairbydesign.nl/ontology/health_or_disease_status_of_specific_host) |

