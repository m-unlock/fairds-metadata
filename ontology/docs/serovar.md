
# Term: serovar
    
serological variety of a species (usually a prokaryote) characterized by its antigenic properties


|              |                                    |
|------------------|-----------------------------------------|
| URL             | [fairds:serovar](http://fairbydesign.nl/ontology/serovar) |

