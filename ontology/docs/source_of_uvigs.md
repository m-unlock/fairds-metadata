
# Term: source of UViGs
    
Type of dataset from which the UViG was obtained


|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `(isolate microbial genome|metagenome \(not viral targeted\)|metatranscriptome \(not viral targeted\)|microbial single amplified genome \(SAG\)|other|sequence-targeted RNA metagenome|sequence-targeted metagenome|viral fraction RNA metagenome \(RNA virome\)|viral fraction metagenome \(virome\)|viral single amplified genome \(vSAG\))`                              |
| Example         | `metagenome (not viral targeted)`                             |
| URL             | [fairds:source_of_uvigs](http://fairbydesign.nl/ontology/source_of_uvigs) |

