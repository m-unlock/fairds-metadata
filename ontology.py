# Code to generate the ontology for the FAIR Data Station

# Check if ontology folder exists
import os

# Move to the directory where this script is located
os.chdir(os.path.dirname(__file__))

# Remove the site folder
os.system("rm -rf ontology/site")
print("Removed site folder")

if not os.path.exists("ontology"):
    os.mkdir("ontology")
    print("Created ontology folder")
if not os.path.exists("ontology/docs"):
    os.mkdir("ontology/docs")
    print("Created ontology/docs folder")

for line in open("terms.tsv"):
    if len(line.strip()) == 0:
        continue
    # Item label	Value syntax	Example	Preferred unit	URL	Definition
    label, syntax, example, unit, url, definition = line.split("\t")
    url_ignores = ["w3id.org"]
    ignore = False
    for url_ignore in url_ignores:
        if url_ignore in url:
            ignore = True
            break
    if ignore: continue
    if len(url) == 0:
        url_label = label.replace(" ", "_").lower().replace("/", "_")
        # Turn into a markdown file using the markdown library
        markdown = f"""
# Term: {label}
    
{definition}

|              |                                    |
|------------------|-----------------------------------------|
| Syntax          | `{syntax}`                              |
| Example         | `{example}`                             |
| Unit            | `{unit}`                                |
| URL             | [fairds:{url_label}](http://fairbydesign.nl/ontology/{url_label}) |

"""
        # Removing rows that have empty values
        for m in markdown.split("\n"): 
            if "``" in m: 
                markdown = markdown.replace(m + "\n", "")
                print(markdown)
        
        label = label.replace(" ", "_").lower().replace("/", "_")
        with open(f"ontology/docs/{label}.md", "w") as f:
            f.write(markdown)
        print(f"Created {label}.md")

# Create the mkdocs.yml file
mkdocs = """
site_name: FAIR Data Station Ontology
nav:
"""
for file in sorted(os.listdir("ontology/docs/")):
    if file.endswith(".md"):
        mkdocs += f"  - {file.replace('.md', '')}: {file}\n"

mkdocs += """
theme:
  name: material
  features:
    - navigation.instant
  hide:
    - navigation
"""

with open("ontology/mkdocs.yml", "w") as f:
    f.write(mkdocs)

print("Created mkdocs.yml")

# Move into the ontology folder
os.chdir("ontology")

# Build the mkdocs site
os.system("mkdocs build")

# Copy the site content to the fairbydesign folder
import shutil

# Obtain location of this script
path = os.path.dirname(__file__)
# Remove the existing ontology folder
shutil.rmtree(path + "/../fairbydesign/public/ontology", ignore_errors=True)
# Copy the new ontology folder
shutil.copytree(path + "/ontology/site", path + "/../fairbydesign/public/ontology")
        