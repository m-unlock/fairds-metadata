import pandas as pd
import os

# Load all TSV files in the current directory into a dictionary of pandas DataFrames
tsv_files = ['regex.tsv', 'terms.tsv', 'Investigation.tsv', 'Study.tsv', 'ObservationUnit.tsv', 'Sample.tsv', 'Assay.tsv']

dataframes = {os.path.splitext(os.path.basename(file))[0]: pd.read_csv(file, sep='\t') for file in tsv_files}

# Create an Excel writer object
writer = pd.ExcelWriter('metadata.xlsx')

# Write each DataFrame to a separate sheet in the Excel file
for sheet_name, df in dataframes.items():
    df.to_excel(writer, sheet_name=sheet_name, index=False)

# Save the Excel file
writer.close()
