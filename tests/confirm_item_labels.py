import pytest

item_labels = set()
with open("terms.tsv", "r") as term_file:
    item_label_index = 0 #term_file.next().split("\t").index("Item label")

    for line in term_file:
        item_labels.add(line.split("\t")[item_label_index].lower())
    
files = ["Assay.tsv", "Sample.tsv"]

for file in files:
    with open(file, "r") as read_file:
        item_label_index = 2 #read_file.next().split("\t").index("Item label")
        
        for line in read_file:
            print(line.split("\t")[item_label_index])
            assert line.split("\t")[item_label_index].lower() in item_labels
 
