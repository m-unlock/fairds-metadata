from openpyxl import load_workbook
import pandas as pd

# Load the Excel workbook
wb = load_workbook("metadata.xlsx")

# Iterate through each sheet in the workbook
for sheet_name in wb.sheetnames:
    ws = wb[sheet_name]
    
    # Extract headers from the first row
    headers = [cell.value for cell in ws[1]]  # Assuming the first row contains the headers
    
    # Create a list to store the data
    data = []
    for row in ws.iter_rows(min_row=2, values_only=False):  # Start from the second row for data
        row_data = []
        for cell in row:
            if isinstance(cell.value, bool):  # Check if cell value is a boolean
                # Keep the boolean value as-is or convert to string if needed
                row_data.append(str(cell.value).lower())  # Or `row_data.append(str(cell.value))` for 'True'/'False'
            elif cell.number_format in ['0%', '0.00%']:  # Check if cell format is percentage
                if cell.value is not None:
                    # Convert to percentage string
                    row_data.append(f"{cell.value * 100:.2f}%")
                else:
                    row_data.append(None)
            else:
                # Keep the original value for non-percentage cells
                row_data.append(cell.value)
        data.append(row_data)
    
    # Convert to a DataFrame
    df = pd.DataFrame(data, columns=headers)  # Use the headers for column names
    
    # Export the sheet to a TSV file
    df.to_csv(f"{sheet_name}.tsv", sep="\t", index=False)
